Report 22. Juni


* Wiederholtes Erstellen von Datenbanken für lokale BLAST-Suche
* Ausprobieren von Alignments - Proteinsequenz blastp gegen die erstellten Datenbank
* Umwandeln der BLAST Datei mit mview in fasta und msf file
* zum Teil betrachten der Alignments mit Aliview und SplitTrees (öffnet .fa files nicht, sondern msf file)

TODOs für morgen:
- suchen Repräsentanten mit guten scores aus mit denen wir z.B. mit clustalo ein Multiples Alignment erstellen wollen um übersichtlichere und sichere Ergebnisse in SplitTrees zu bekommen
Report 22. Juni

* Proteinsequenz UTX [homo sapiens von nbci] hat keinerlei Matches in den vorgegebenen Species, daher wird das Protein im folgenden ignoriert

* Recherche: was bedeuten parallele Kanten in SplitsTree
https://www.microbialsystems.cn/en/post/phylogenetic_network/
https://academic.oup.com/bioinformatics/article/14/1/68/267267

![JMJD](JMJD3_06_24.png) 

* Suchen der in interpro gefundenen Proteindomänen in pfam, download HMMs
``` 
# db erstellen
hmmpress fn3.hmm
#sequencz gegen db suchen
hmmscan -o fn3_myprot.txt --tblout fn3_myprot_table.txt fn3.hmm myprotseq.fasta
```
| protein| domain | wo? | source |
| ---      |  ---  | --- | --- |
|JMJD3/ KDM6B|JmjC| in sequence 1339 - 1502| Interpro |
| UTX (Drosophila m.) | JmjC| 826-989 | InterPro |
| UTX (Drosophila m.) | Tetratricopeptide-like helical domain| 26-271, 272-429 | InterPro |
| UTX (Drosophila m.) | Tetratricopeptide repeat-containing domain| 114-407| InterPro |
| UTX (Drosophila m.) | Tetratricopeptide repeat| 114-147,152-185,189-222, 265-305, 306-339, 340-373, 374-407 | InterPro |
| UTX (Drosophila m.) |JmjC| 826-989| UniProt|
|HCF1 (human) NC_000023.11:c153972360-153947556 | KELCH MOTIF | in sequenz 32-39 (InterPro) | InterPro |
|HCF1 (human) NC_000023.11:c153972360-153947556 | Kelch beta propeller | in sequenz 41-110 | InterPro |
|HCF1 (human) NC_000023.11:c153972360-153947556 | Fibronectin Type III Domain (fn3) | in sequenz 364-536 | InterPro |
|HCF1 (human) NC_000023.11:c153972360-153947556 | Predicted Domain | 585-1079 | InterPro |
|HCF1 (human) NC_000023.11:c153972360-153947556 | Predicted Domain | 1259-1546 | InterPro |
|HCF1 (human) NC_000023.11:c153972360-153947556 | fn3 | 1795-1888 | Interpro |
|HCF1 (human) NC_000023.11:c153972360-153947556 | KELCH 1 | in sequenz 44-89  | UniProt |
|HCF1 (human) NC_000023.11:c153972360-153947556 | KELCH 2 | in sequenz 93-140  | UniProt |
|HCF1 (human) NC_000023.11:c153972360-153947556 | KELCH 3 | in sequenz 148-194 | UniProt |
|HCF1 (human) NC_000023.11:c153972360-153947556 | KELCH 4 | in sequenz 217-265 | UniProt |
|HCF1 (human) NC_000023.11:c153972360-153947556 | KELCH 5 | in sequenz 266-313 | UniProt |
|HCF1 (human) NC_000023.11:c153972360-153947556 | fn3 1 | in sequenz 366-466 | UniProt
|HCF1 (human) NC_000023.11:c153972360-153947556 | fn3 2 | in sequenz 1797-1888 | UniProt |
|HCF1 (human) NC_000023.11:c153972360-153947556 | fn3 3 | in sequenz 1890-2006 | UniProt |
|HCF1 (human) NC_000023.11:c153972360-153947556 | HCF Repeat 1-8 | in sequenz ~1010-1888 | UniProt |
|HCF1 (human) NC_000023.11:c153972360-153947556 | Immunoglobulin-like fold | 1805-1888 | InterPro |
|HCF1 (human) NC_000023.11:c153972360-153947556 | Immunoglobulin-like fold | 1889-2017 | InterPro |

* Zusätzliche Recherche zu Grundlegenden Funktionen der Domänen, vorwiegend auf InterPro

* weitere predicted domains für JMJD3/KDM6B (http://www.genome3d.eu/uniprot/id/O15054/annotations), können allerdings nicht unter den angegebenen Namen eindeutig in pfam gefunden werden.

* Download HMMs und experimentieren mit Suchanfragen von Domänen gegen Proteinsequenz / Datenbank

* Idee --> Predicted Domänen aus Proteinsequenz schneiden --> mit BLAST gegen lokale Datenbank suchen --> Alignment nehmen, mit hmm-tools von multiplen Sequenzalignment zu profil hmm umwandeln --> Modell von Domäne erstellt --> Suche erneut in Datenbank nach Domäne

* bzgl Datenmanagement --> Verschiedene Ordnerstrukturen, die sich nach den runs der programme und datentypen aufteilen --> blastdb/proteome, blastdb/genome, blastruns/ergebnis.BLAST, hmm/kelch1 ... . Wir haben für die Blast datenbank alle Protein-und Genomsequenzen appended und zusätzlich jeder nSequenz/pSequez im concatinierten File eine ID zugewiesen --> 1-n. Das concatted fasta hat dann folgende Struktur --> >ID-quellenkennummer(z. B NC_000356) ...zusätzliche metainformationen aus quelle (genname, position in sequenz etc)... . 

* Idee morgen --> Gute Treffer mit hmmscan auf der proteindatenbank in SplitsTree erkunden --> Sehen wie sich das entstandene SplitsNetwork verändert --> evtl dafür Domäne aus Proteinsequenz schneiden, z.B fn3, mit clustalo auf die mit hmmscan gefundenen Domänen dazu alignen und SplitsTree erkunden.

* Alternativ zu dem "Erkunden der Domänen mit Splitstree" morgen --> Ergebnisse der BLAST suchen bezüglich e-wert, Coverage, Identität, Länge Clustern (,Position in Sequenz?) (Naiv oder evtl. mit HDBSCAN). Dann aus jedem Cluster eine Kandidatensequenz nehmen und die entstehenden Splitstrees untersuchen 


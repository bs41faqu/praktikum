Report 26. Juni

Lena: 
* Da Schüttengruber sagt, UTX ist nicht in Filasterea, Ichthyosporea, Fungi oder Viridiplantae, überprüfe ich, ob in den Blasthits keine der Species, für die wir Proteomdaten haben, vorhanden ist. Dabei stellt sich heraus, dass SCER und NCRA Proteine (CYC8 und RCM-1, resp.) mit gute Alignments in der Region mit TPR-Domänen mit dem Drosophila-UTX Proteom haben.
* Recherche bei UnitPro über ebend. Proteine: beiden wird eine histone demethylase activity (H3-K27 secific) zugeschrieben, diese Einordnung ist jedoch nur "Inferred from biological aspect of ancestor"
https://www.uniprot.org/uniprot/P14922 https://www.uniprot.org/uniprot/V5INE2
* Für JMJD3 lassen sich keine hits im Filasteria, Ichthyosporea, Fungi oder Viridiplantae Proteom finden.
* 


HCF1
* Sichtung homologer/paraloger Genome für HCF1 in verschiedenen Gruppen (bilateria etc) und erstellen von MSA mit clustalo --> Training HMM mit Modell und Suche in Filasterea
* Sammeln von HCF-repeat motifs in HCF1 homologen für Hmm Modell
* Erstellen python script für Suche von query-Domänenkombinationen in Proteinsequenzen mit hmmscan
```+----------------+----------------------------------------------------------------------------------------------+
| XP_004365164.1 | | Kelch_2    | Kelch_3    | Kelch_4    | Kelch_5    | Kelch_6    | RAG2       | Kelch_1    | |
|                | |------------|------------|------------|------------|------------|------------|------------| |
|                | | 0.97       | 0.95       | 0.96       | 0.91       | 0.94       | 0.7        | 0.95       | |
|                | | (308, 348) | (475, 520) | (308, 356) | (462, 500) | (308, 349) | (229, 382) | (308, 348) | |
+----------------+----------------------------------------------------------------------------------------------+
| XP_004365629.1 | | Kelch_2    | Kelch_3    | Kelch_4    | Kelch_5    | Kelch_6    | Kelch_1    |              |
|                | |------------|------------|------------|------------|------------|------------|              |
|                | | 0.95       | 0.93       | 0.93       | 0.95       | 0.87       | 0.92       |              |
|                | | (283, 325) | (295, 346) | (230, 268) | (280, 318) | (393, 443) | (285, 324) |              |
+----------------+----------------------------------------------------------------------------------------------+
| XP_004366028.2 | | Kelch_2    | Kelch_3   | Kelch_4   | Kelch_5    | Kelch_6    | Kelch_1    |                |
|                | |------------|-----------|-----------|------------|------------|------------|                |
|                | | 0.95       | 0.95      | 0.96      | 0.91       | 0.95       | 0.97       |                |
|                | | (210, 255) | (78, 128) | (65, 119) | (208, 249) | (211, 255) | (210, 255) |                |
+----------------+----------------------------------------------------------------------------------------------+
| XP_011269991.1 | | Kelch_2    | Kelch_3   | Kelch_4    | Kelch_5   | Kelch_6    | RAG2       | Kelch_1   |    |
|                | |------------|-----------|------------|-----------|------------|------------|-----------|    |
|                | | 0.95       | 0.94      | 0.94       | 0.97      | 0.92       | 0.8        | 0.95      |    |
|                | | (172, 215) | (77, 124) | (222, 267) | (63, 101) | (222, 267) | (284, 310) | (65, 107) |    |
+----------------+----------------------------------------------------------------------------------------------+
| XP_011270786.1 | | Kelch_2    | Kelch_3    | Kelch_4    | Kelch_5    | Kelch_6    | Kelch_1    |              |
|                | |------------|------------|------------|------------|------------|------------|              |
|                | | 0.96       | 0.9        | 0.91       | 0.95       | 0.95       | 0.95       |              |
|                | | (346, 388) | (461, 506) | (346, 390) | (344, 383) | (295, 340) | (347, 388) |              |
+----------------+----------------------------------------------------------------------------------------------+
| XP_004343870.2 | | Kelch_4    | Kelch_5    | Kelch_6    | RAG2       | Kelch_1    |                           |
|                | |------------|------------|------------|------------|------------|                           |
|                | | 0.83       | 0.87       | 0.9        | 0.81       | 0.91       |                           |
|                | | (362, 402) | (357, 394) | (362, 402) | (296, 379) | (316, 346) |                           |
+----------------+----------------------------------------------------------------------------------------------+
```

--> sucht nach einer Menge von Domänen in Fasta File mit hmmscan und gibt die wahrscheinlichsten Hit jeder Domäne mit konfidenz und sequenzposition an --> Erweiterung des query-prozesses mit verschiedenen Reihenfolge des auftretens der Domänen und maximaler Distanz zwischen Domänen (z.B K1-K2-K3-fn31-fn32 mit maximaler Distanz 100 zwischen Domänen) und scoring der Matches anhand e-values, confidence und score 

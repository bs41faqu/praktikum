import getopt, sys, glob, subprocess
from constraint import *
from tabulate import tabulate
from Bio import SeqIO

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:p:o:vs:r:a:g", ["help","input=","proximity=" "output=","outputsequence=","outputdom=","outputhmm=","sequence=","report=","assemble=","goverlap"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    output = ""
    outfiles = []
    verbose = False
    input_dir = ""
    proximity = 0
    sequence = ""
    result_json = {}
    report = ""
    assemble = False
    overlap = False
    assemble_min = 1
    sequence_out = ""
    hmm_out = ""
    outputsequencedom = 1

    for o, a in opts:
        if o == "-v":
            verbose = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-s", "--sequence"):
            sequence = a
        elif o in ("-o", "--output", "-os","--outputsequence","-oh","--outputhmm","--outputdom"):
            if(o=="-o" or o == "--output"):
                output = a
            elif(o=="--outputhmm"):
                hmm_out = a
            elif(o=="--outputsequence"):
                sequence_out = a
            elif(o=="--outputdom"):
                outputsequencedom = int(a)
        elif o in ("-i","--input","--inputdir"):
            input_dir = a
        elif o in ("-p", "--proximity"):
            proximity = a
        elif o in ("-report","-r"):
            report = a
        elif o in ("-assemble","-a"):
            assemble = True
            assemble_min = int(a)
            if(assemble_min < 0):
                assemble_min = 1
        elif o in ("-gooverlap"):
            overlap = True
        else:
            assert False, "unhandled option"
    in_list = glob.glob(input_dir+"*.hmm")
    for f in in_list:
        # print(output)
        # print(input_dir)
        # print(f)
        # print(f.split("/")[0])
        # print(f.split("/")[1])
        # print(len(f.split("/")))
        # print(f.split("/")[len(f.split("/"))-1])
        outfiles += [output+f.split("/")[len(f.split("/"))-1]+".csv"]
        #print("hmmscan --domtblout "+output+f.split("/")[len(f.split("/"))-1]+".csv "+ f + " " + sequence)
        subprocess.call("hmmscan --cpu 4 -o " + output+f.split("/")[len(f.split("/"))-1]+".aln" + " --domtblout "+output+f.split("/")[len(f.split("/"))-1]+".csv "+ f + " " + sequence,shell=True)

    if(assemble == False):
        for f in outfiles:
            with open(f,"r") as file:
                content = file.read()
                table = content.split("#\n")[0]
                table_rows = table.split("\n")
                table_len = len(table_rows)
                for index in range(3,table_len-1):
                    #print(table_rows[index].split(None,23))
                    row = table_rows[index].split(None,23)
                    dom = row[0]
                    protein = row[3]
                    asc = row[1]
                    conf = float(row[21])
                    seqf = int(row[17])
                    seqt = int(row[18])
                    #print(dom + " -- " + protein + " -- " + asc + " -- " + conf)
                    if( protein in result_json.keys() ):
                        for i in range(0,len(result_json[protein]["domains"])):
                            if( dom == result_json[protein]["domains"][i][0] ):
                                #print("going comp")
                                if (result_json[protein]["domains"][i][1]<conf):
                                    result_json[protein]["domains"][i] = (dom,conf,(seqf,seqt))
                                    break
                            elif(i==len(result_json[protein]["domains"])-1):
                                result_json[protein]["domains"] += [(dom,conf,(seqf,seqt))]
                    else:
                        result_json[protein] = {}
                        result_json[protein]["domains"] = [(dom,conf,(seqf,seqt))]
        print("#[ALIGN OK]")
        if(report != ""):
            with open(report,"w+") as f:
                #list_domains = []
                tablist = []
                keylist = []
                for k in result_json.keys():
                    hlist = []
                    formlist = []
                    seqlist = []
                    seqlist2 = []
                    for t in result_json[k]["domains"]:
                        hlist += [str(t[0])]
                        formlist += [t[1]]
                        seqlist += [str(t[2][0])]
                        seqlist2 += [str(t[2][1])]
                    f.write(k + "\t" + str(result_json[k]["domains"])+"\n")
                    #list_key += [[k,str(result_json[k]["domains"])]]
                    st = tabulate([formlist,seqlist,seqlist2],headers=hlist, tablefmt="fancy_grid")
                    tablist += [[k[0:70],"\n"+st+"\n"]]
                    #print(tabulate( [[k,st],[k,st]] ))
                    #, headers=["seqid","domains"] ) )
                    #list_domains += [str(result_json[k]["domains"])]
                #print(tabulate(list_key,headers=["protein query","domain hits"]))
                print(tabulate( tablist , headers = [ ["seqid"],["domains"] ], tablefmt="fancy_grid" ))
        else:
            #list_domains = []
            tablist = []
            keylist = []
            for k in result_json.keys():
                hlist = []
                formlist = []
                seqlist = []
                seqlist2 = []
                #print()
                for t in result_json[k]["domains"]:
                    hlist += [str(t[0])]
                    formlist += [t[1]]
                    seqlist += [str(t[2][0])]
                    seqlist2 += [str(t[2][1])]
                #f.write(k + "\t" + str(result_json[k]["domains"])+"\n")
                #list_key += [[k,str(result_json[k]["domains"])]]
                st = tabulate([formlist,seqlist,seqlist2],headers=hlist, tablefmt="fancy_grid")
                tablist += [[k[0:70],"\n"+st+"\n"]]
                #print(tabulate( [[k,st],[k,st]] ))
                #, headers=["seqid","domains"] ) )
                #list_domains += [str(result_json[k]["domains"])]
            #print(tabulate(list_key,headers=["protein query","domain hits"]))
            print(tabulate( tablist , headers = [ ["seqid"],["domains"] ], tablefmt="fancy_grid" ))
    else:
        for f in outfiles:
            #print(f)
            with open(f,"r") as file:
                content = file.read()
                table = content.split("#\n")[0]
                table_rows = table.split("\n")
                #print(len(table_rows))
                table_len = len(table_rows)
                for index in range(3,table_len-1):
                    #print(table_rows[index].split(None,23))
                    row = table_rows[index].split(None,23)
                    dom = row[0]
                    protein = row[3]
                    asc = row[1]
                    conf = float(row[21])
                    seqf = int(row[17])
                    seqt = int(row[18])
                    #print(dom + " -- " + protein + " -- " + str(asc) + " -- " + str(conf))
                    if( protein in result_json.keys() ):
                        if( dom in result_json[protein]["domains"].keys()):
                            result_json[protein]["domains"][dom] += [(conf,seqf,seqt)]
                        else:
                            result_json[protein]["domains"][dom] = {}
                            result_json[protein]["domains"][dom] = [(conf,seqf,seqt)]
                    else:
                        #print("added_domain!!")
                        result_json[protein] = {}
                        result_json[protein]["domains"] = {}
                        result_json[protein]["domains"][dom] = {}
                        result_json[protein]["domains"][dom] = [(conf,seqf,seqt)]
        print(result_json)
        for k in result_json.keys():
            result_json[k]["assembly_solutions"] = {}
            for d in range (assemble_min,len(result_json[protein]["domains"])+1):
                assembly_problem = Problem()
                domain_pieces = []
                for domainentry in result_json[k]["domains"]:
                    for t in result_json[k]["domains"][domainentry]:
                        domain_pieces += [(domainentry,t[0],t[1],t[2])]
                #print("---------------------")
                #print(domain_pieces)
                #print("---------------------")
                for vv in range(0,d):
                    assembly_problem.addVariable("v"+str(vv), domain_pieces)
                #assembly_problem.addVariables( ["v"+str(ind) for ind in range(0,d)] , [domain_pieces for ind2 in range(0,d)] )
                if(d!=1):
                    for j in range(0,d):
                        for l in range(0,d):
                            if(j==l):
                                continue
                            elif j<l:
                                #assembly_problem.addConstraint(lambda v1,v2 : print(v1), ["v"+str(j),"v"+str(l)])
                                if(overlap):
                                    assembly_problem.addConstraint(lambda v1,v2 : v1[2] < v2[3] and v1[0]!=v2[0],["v"+str(j),"v"+str(l)])
                                else:
                                    assembly_problem.addConstraint(lambda v1,v2 : v1[2]<v2[3] and v1[3]<=v2[2] and v1[0]!=v2[0],["v"+str(j),"v"+str(l)])
                            #elif j>k
                            #    problem.addConstraint(lambda,("v"+j,"v"+k))
                result_json[k]["assembly_solutions"][d] = []
                result_json[k]["assembly_solutions"][d] = assembly_problem.getSolutions()
        #print(result_json)

        tablist = []
        keylist = []
        for k in result_json.keys():
            hlist = []
            formlist = []
            seqlist = []
            seqlist2 = []
            tabular_ranking_list = []
            for t in result_json[k]["domains"]:
                hlist += [t]
                max_conf = max(result_json[k]["domains"][t],key=lambda item:item[0])
                formlist += [max_conf[0]]
                seqlist += [max_conf[1]]
                seqlist2 += [max_conf[2]]
            #list_key += [[k,str(result_json[k]["domains"])]]
            st = tabulate([formlist,seqlist,seqlist2],headers=hlist, tablefmt="fancy_grid")
            #tablist += [[k[0:70],"\n"+st+"\n"]]
            #rank my solutions
            for s in result_json[k]["assembly_solutions"]:
                    if(result_json[k]["assembly_solutions"][s]):
                        combination_list = []
                        ranking_list = []
                        for ix in range(assemble_min,len(result_json[k]["assembly_solutions"][s])):
                            dom_list = []
                            for dm_ix in range(0,s):
                                dom_list = result_json[k]["assembly_solutions"][s][ix]
                            score = 0
                            for dm in dom_list:
                                score += dom_list[dm][1]
                            ranking_list += [score/len(dom_list)]
                            combination_list += [dom_list]
                    else: continue
                    ranking = 0
                    while(True):
                        found = False
                        if(ranking>9):
                            break
                        ranking = ranking+1
                        if(len(combination_list) == 0):
                            break
                        else:
                            current_max_score = max(ranking_list)
                            poplist = []
                            strt_ind = len(ranking_list)-1
                            for ix2 in range(0,len(ranking_list)):
                                if(current_max_score==ranking_list[strt_ind-ix2]):
                                    #print(str(combination_list[ix2]) + " --- " + str(ranking_list[ix2]))
                                    if(found == False):
                                        row_headers = ["score"]
                                        row_seqf = [ranking_list[strt_ind-ix2]]
                                        row_seqt = [ranking_list[strt_ind-ix2]]
                                        row_seqconf = [ranking_list[strt_ind-ix2]]
                                        for var in combination_list[strt_ind-ix2]:
                                            #print(var)
                                            #print(combination_list[ix2][var])
                                            row_headers += [combination_list[strt_ind-ix2][var][0]]
                                            row_seqconf += [combination_list[strt_ind-ix2][var][1]]
                                            row_seqf += [combination_list[strt_ind-ix2][var][2]]
                                            row_seqt += [combination_list[strt_ind-ix2][var][3]]

                                        tabular_ranking_list += [tabulate( [row_seqconf,row_seqf,row_seqt], headers = row_headers,tablefmt = "fancy_grid" )]
                                        found = True
                                    ranking_list.pop(strt_ind-ix2)
                                    combination_list.pop(strt_ind-ix2)
            tabstring = "\n" + st + "\n"
            for tab in tabular_ranking_list:
                tabstring += tab + "\n"
            tablist += [[k[0:70],tabstring]]
        print(tabulate( tablist , headers = [ ["seqid"],["domains"] ], tablefmt="fancy_grid" ))

    hmm_set = set()
    seq_set = set()
    print(hmm_out)
    if(hmm_out != ""):
        for k in result_json:
            for d in result_json[k]["domains"]:
                if(assemble==False):
                    hmm_set.add(d[0])
                else:
                    hmm_set.add(d)
        for x in hmm_set:
            for f in in_list:
                subprocess.call("hmmfetch " + f + " " + x + " > " + hmm_out + x + ".hmm", shell=True)
                subprocess.call("hmmpress " + hmm_out + x + ".hmm", shell = True)
    if(sequence_out != ""):
        records = SeqIO.parse(sequence,"fasta")
        new_records = []
        with open(sequence_out,"w+") as out_fasta:
            for r in records:
                if(r.id in result_json.keys()):
                    if(assemble==False):
                        if(len(result_json[r.id]["domains"])>=outputsequencedom):
                            new_records += [r]
                    else:
                        if(len(result_json[r.id]["domains"].keys())>=outputsequencedom):
                            new_records += [r]
            SeqIO.write(new_records,out_fasta,"fasta")
            print("#[WROTE " + str(len(new_records)) + " SEQUENCES TO FASTA]")

        #for p in result_json.keys():
        #    for s in result_json[p]["assembly_solutions"]:
        #        if(result_json[p]["assembly_solutions"][s]):
                    #for l in result_json[p]["assembly_solutions"][s]:
                    #print(str(s) + " --- " + str(result_json[p]["assembly_solutions"][s]))

if __name__ == "__main__":
    main()

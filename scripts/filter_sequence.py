from Bio import SeqIO
import getopt, sys

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:q:f:", ["help","input=", "output=","query=","file="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    output = ""
    query_id = ""
    input = ""
    in_id_file = ""

    for o, a in opts:
        if o == "-v":
            verbose = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-f","--file"):
            in_id_file = a
        elif o in ("-q","--query"):
            query_id = a
        elif o in ("-s", "--sequence"):
            sequence = a
        elif o in ("-o", "--output", "-os","--outputsequence","-oh","--outputhmm","--outputdom"):
            output = a
        elif o in ("-i","--input","--inputdir"):
            input = a
        else:
            assert False, "unhandled option"

    if(in_id_file != ""):
        id_list = []
        with open(in_id_file,"r") as f:
            cont = f.read().split("\n")
            for id in cont:
                if(id == ""):
                    continue
                id_list += [id]
        with open(output,"w+") as of:
            records = SeqIO.parse(input,"fasta")
            out_records = []
            for r in records:
                for id2 in id_list:
                    #print(str(r.id[0:len(id2)]))
                    #print(id2)
                    if(str(r.id[0:len(id2)])==id2):
                        out_records += [r]
                        #SeqIO.write(r,output,"fasta")
                        print("[WROTE" +str(r.id) +  "]")
            SeqIO.write(out_records,output,"fasta")

    else:
        with open(output,"w+") as f:
            records = SeqIO.parse(input,"fasta")
            for r in records:
                if ( r.id == query_id ):
                    SeqIO.write(r,output,"fasta")
                    break
if __name__ == "__main__":
    main()

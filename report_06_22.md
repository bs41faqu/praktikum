Report 22. Juni

* Protein Recherche (Ergbenisse siehe unten)
* Herunterladen Proteome und Genome, speichern per SSH auf Praktikums-PC
* Herunterladen "unsrerer" Proteinsequenzen, wir haben uns für Homo sapiens als Referenz entschieden, alle "unsere" Proteindaten sind von ncbi runtergeladen
* Bereinigen/ Aufbereiten einiger Proteinsequenzen und Genome --> inkludieren von zoologischen Bezeichnungen anstelle m.xxxx sowie erstellung von Protein/Genomsammlungen in einem FASTA --> Uns ist nach dem erstellen der ersten Alignments aufgefallen, dass manche Protein/sequenzdaten keine sprechenden Taxa bezeichnungen enthalten. 
* Erstellen von Datenbanken für lokale BLAST-Suche aus den gemergeten .fa mit
```makeblastdb -in ../../data_collected/proteomes/all_proteomes.faa.gz -parse_seqids -dbtype prot -title proteomes_blastdb_22_06 -out proteomes_blastdb_22_06 ; makeblastdb -in ../../data_collected/genomes/all_genomes.faa.gz -parse_seqids -dbtype nucl -title genomes_blastdb_22_06 -out genomes_blastdb_22_06```
* Ausprobieren von Alignments - Proteinsequenz und DNA sequenz unserer Gene mit blastn,blastp und blastx gegen die erstellten Datenbanken matchen (proteomdb und genomdb) mit 
``` blastp -query ../../data_bruno/proteins/HCF1_protein.fasta -d proteomes_blastdb_22_06 -o ../../HCF1_protein_align.aln ;  blastn -query ../../data_bruno/proteins/HCF1_sequence.fasta -d genomes_blastdb_22_06 -o ../../HCF1_sequence_align.aln```


# Protein Recherche
## UTX
also known as lysine-specific demethylase 6A, KDM6A
https://www.ebi.ac.uk/interpro/entry/InterPro/IPR029518/

## JMJD3 
(also known as KDM6B) --> together with UTX, UTY members JmjC subfamily of histone lysine demethylase family --> KDM; katalysieren demethylierung H3K27, repressors
https://www.ebi.ac.uk/interpro/entry/InterPro/IPR029518/

## JMJD2 
JMJD2 proteins, also termed lysine-specific demethylase 4 histone demethylases (KDM4), have been implicated in various cellular processes including DNA damage response, transcription, cell cycle regulation, cellular differentiation, senescence, and carcinogenesis. They selectively catalyze the demethylation of di- and trimethylated H3K9 and H3K36. This model contains only three JMJD2 proteins, JMJD2A-C, which all contain jmjN and jmjC domains in the N-terminal region, followed by a Cys4HisCys3 canonical PHD finger, a non-canonical extended PHD (ePHD) finger, Cys2HisCys5HisCys2His, and a Tudor domain. JMJD2D is not included in this family, since it lacks both PHD and Tudor domains and has a different substrate specificity. JMJD2A-C are required for efficient cancer cell growth. This model corresponds to the Cys4HisCys3 canonical PHD finger
https://www.ebi.ac.uk/interpro/entry/cdd/CD15493/

## JMJD1 
also known as KDM3A; lysine demethylase 3A (NOT FINDABLE INTERPRO) 
https://www.ncbi.nlm.nih.gov/gene/55818

## HCF1
Host Cell Factor 1 --> his entry includes Host cell factor 1 (HCF1) and Host cell factor 2 (HCF2) from humans. They contain an N-terminal kelch domain and a C-terminal FnIII domain. However, HCF2 is smaller than HCF-1, lacking the complete central region including the HCF1 specific repeats and as a result is not subject to proteolytic processing
[1]. This entry also includes their Drosophila melanogaster homologue, dHCF, which is involved in both activation and repression of transcription during fly development
[2].
HCF1 is associated with the activation and repression of gene expression. It is brought to specific promoters by association with DNA-sequence-specific transcription factors such as Sp1, GABP, YY1, Ronin/THAP11, and E2F1 and E2F4
[2]. HCFC1 recruits and is a part of several different complexes, including the SET1 histone methyltransferase complex (transcription activation), the SIN3 histone deacetylase complex (transcription repression)
[3], the THAP1/THAP3-HCFC1-OGT complex (required for the regulation of the transcriptional activity of RRM1)
[4], and the NSL complex (acetylation of nucleosomal histone H4)
[5].
HCF2 is involved in activation of differentiation and morphogenesis gene expression programs, and in parallel in inhibition of cellular growth and metabolism
[6]. (https://www.ebi.ac.uk/interpro/entry/InterPro/IPR043536/)

## PTIP
??? This protein domain family is the C-terminal domain of PAXIP1-associated-protein-1, which also goes by the name PTIP-associated protein 1. This family of proteins is found in eukaryotes. The function of this protein is to localise at the site of DNA damage and form foci with PTIP at the DNA break point. Furthermore, studies have shown that depletion of PA1 increases cellular sensitivity to ionizing radiation. Proteins in this family are typically between 122 and 254 amino acids in length
[PMID:19124460]. PTIP is apparently synonymous for PAXIP1 – PAX interacting protein 1 in Homo sapiens (human) (Also known as: CAGF28, CAGF29, PACIP1, PAXIP1L, PTIP, TNRC2) according to NCBI (https://www.ncbi.nlm.nih.gov/gene/?term=PTIP+AND+homo+sapiens)


Alignment for PTIP with blastp 

```cat ../../blastruns/HCF1_protein_proteomes_blast_db_22_06.aln 
BLASTP 2.2.18+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.



Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: proteomes_blastdb_23_06
           581,817 sequences; 178,919,053 total letters



Query=  NP_005325.2 host cell factor 1 [Homo sapiens]
Length=2035
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

lcl|m.163238  g.163238  ORF g.163238 m.163238 type:5prime_partial...   121    6e-26
lcl|m.230095  g.230095  ORF g.230095 m.230095 type:5prime_partial...   118    4e-25
lcl|m.275446  g.275446  ORF g.275446 m.275446 type:complete len:5...   108    4e-22
lcl|m.234512  g.234512  ORF g.234512 m.234512 type:complete len:6...   100    1e-19
lcl|m.136234  g.136234  ORF g.136234 m.136234 type:5prime_partial...  99.4    2e-19
lcl|m.136096  g.136096  ORF g.136096 m.136096 type:complete len:9...  99.4    3e-19
lcl|m.47255  g.47255  ORF g.47255 m.47255 type:complete len:522 (...  98.2    5e-19
lcl|m.234536  g.234536  ORF g.234536 m.234536 type:5prime_partial...  97.4    8e-19
lcl|m.19860  g.19860  ORF g.19860 m.19860 type:complete len:347 (...  96.7    1e-18
lcl|m.146441  g.146441  ORF g.146441 m.146441 type:complete len:9...  96.3    2e-18
lcl|m.163194  g.163194  ORF g.163194 m.163194 type:internal len:5...  93.6    1e-17
lcl|m.10928  g.10928  ORF g.10928 m.10928 type:5prime_partial len...  93.6    1e-17
lcl|m.106387  g.106387  ORF g.106387 m.106387 type:complete len:5...  91.3    6e-17
lcl|m.304884  g.304884  ORF g.304884 m.304884 type:complete len:7...  88.6    4e-16
lcl|m.221735  g.221735  ORF g.221735 m.221735 type:complete len:5...  87.4    1e-15
lcl|m.309164  g.309164  ORF g.309164 m.309164 type:complete len:4...  87.0    1e-15
lcl|m.165613  g.165613  ORF g.165613 m.165613 type:5prime_partial...  86.7    1e-15
lcl|m.155044  g.155044  ORF g.155044 m.155044 type:internal len:6...  85.9    3e-15
lcl|m.124104  g.124104  ORF g.124104 m.124104 type:internal len:3...  85.5    4e-15
lcl|m.267086  g.267086  ORF g.267086 m.267086 type:5prime_partial...  84.0    1e-14
lcl|m.204321  g.204321  ORF g.204321 m.204321 type:3prime_partial...  83.6    1e-14
lcl|m.88467  g.88467  ORF g.88467 m.88467 type:5prime_partial len...  82.8    2e-14
lcl|m.142632  g.142632  ORF g.142632 m.142632 type:3prime_partial...  82.8    2e-14
lcl|m.7228  g.7228  ORF g.7228 m.7228 type:5prime_partial len:456...  82.0    4e-14
lcl|m.202333  g.202333  ORF g.202333 m.202333 type:complete len:5...  81.3    6e-14
lcl|m.109274  g.109274  ORF g.109274 m.109274 type:complete len:4...  81.3    6e-14
lcl|m.183317  g.183317  ORF g.183317 m.183317 type:5prime_partial...  81.3    7e-14
lcl|m.61082  g.61082  ORF g.61082 m.61082 type:internal len:222 (...  79.0    3e-13
lcl|m.25282  g.25282  ORF g.25282 m.25282 type:complete len:423 (...  78.6    4e-13
lcl|m.39939  g.39939  ORF g.39939 m.39939 type:complete len:510 (...  77.8    7e-13
lcl|m.53162  g.53162  ORF g.53162 m.53162 type:3prime_partial len...  77.4    1e-12
lcl|m.140012  g.140012  ORF g.140012 m.140012 type:5prime_partial...  76.6    2e-12
lcl|m.219050  g.219050  ORF g.219050 m.219050 type:5prime_partial...  76.6    2e-12
lcl|m.125539  g.125539  ORF g.125539 m.125539 type:internal len:4...  75.5    3e-12
lcl|m.300404  g.300404  ORF g.300404 m.300404 type:5prime_partial...  75.5    4e-12
lcl|m.204454  g.204454  ORF g.204454 m.204454 type:5prime_partial...  74.7    6e-12
lcl|m.92774  g.92774  ORF g.92774 m.92774 type:complete len:1031 ...  74.7    7e-12
lcl|m.97663  g.97663  ORF g.97663 m.97663 type:5prime_partial len...  74.7    7e-12
lcl|m.37148  g.37148  ORF g.37148 m.37148 type:internal len:58 (-...  74.3    9e-12
lcl|m.232738  g.232738  ORF g.232738 m.232738 type:complete len:1...  73.9    9e-12
lcl|m.23187  g.23187  ORF g.23187 m.23187 type:5prime_partial len...  73.9    1e-11
lcl|m.109430  g.109430  ORF g.109430 m.109430 type:5prime_partial...  73.2    2e-11
lcl|m.537044  g.537044  ORF g.537044 m.537044 type:complete len:4...  73.2    2e-11
lcl|m.824731  g.824731  ORF g.824731 m.824731 type:5prime_partial...  72.4    3e-11
lcl|m.311306  g.311306  ORF g.311306 m.311306 type:5prime_partial...  72.4    3e-11
lcl|m.23139  g.23139  ORF g.23139 m.23139 type:5prime_partial len...  72.4    4e-11
lcl|m.124725  g.124725  ORF g.124725 m.124725 type:3prime_partial...  72.0    4e-11
lcl|m.633109  g.633109  ORF g.633109 m.633109 type:5prime_partial...  71.2    7e-11
lcl|m.51106  g.51106  ORF g.51106 m.51106 type:5prime_partial len...  71.2    7e-11
lcl|m.107524  g.107524  ORF g.107524 m.107524 type:complete len:3...  71.2    7e-11
lcl|m.196900  g.196900  ORF g.196900 m.196900 type:5prime_partial...  71.2    8e-11
lcl|m.632932  g.632932  ORF g.632932 m.632932 type:5prime_partial...  70.5    1e-10
lcl|m.231006  g.231006  ORF g.231006 m.231006 type:5prime_partial...  70.5    1e-10
lcl|m.125330  g.125330  ORF g.125330 m.125330 type:complete len:5...  70.5    1e-10
lcl|m.271454  g.271454  ORF g.271454 m.271454 type:5prime_partial...  69.3    3e-10
lcl|m.897658  g.897658  ORF g.897658 m.897658 type:5prime_partial...  68.9    3e-10
lcl|m.49889  g.49889  ORF g.49889 m.49889 type:internal len:230 (...  68.9    4e-10
lcl|m.301311  g.301311  ORF g.301311 m.301311 type:complete len:3...  68.6    4e-10
lcl|m.13884  g.13884  ORF g.13884 m.13884 type:complete len:552 (...  68.6    4e-10
lcl|m.176636  g.176636  ORF g.176636 m.176636 type:5prime_partial...  68.2    6e-10
lcl|m.55365  g.55365  ORF g.55365 m.55365 type:5prime_partial len...  68.2    6e-10
lcl|m.247933  g.247933  ORF g.247933 m.247933 type:internal len:3...  67.8    8e-10
lcl|m.78160  g.78160  ORF g.78160 m.78160 type:internal len:518 (...  67.4    9e-10
lcl|m.206285  g.206285  ORF g.206285 m.206285 type:complete len:1...  67.4    1e-09
lcl|m.149821  g.149821  ORF g.149821 m.149821 type:5prime_partial...  67.0    1e-09
lcl|m.132977  g.132977  ORF g.132977 m.132977 type:5prime_partial...  67.0    1e-09
lcl|m.102513  g.102513  ORF g.102513 m.102513 type:5prime_partial...  66.6    2e-09
lcl|m.136296  g.136296  ORF g.136296 m.136296 type:complete len:4...  65.9    3e-09
lcl|m.10045  g.10045  ORF g.10045 m.10045 type:complete len:193 (...  65.9    3e-09
lcl|m.152617  g.152617  ORF g.152617 m.152617 type:5prime_partial...  65.5    4e-09
lcl|m.25535  g.25535  ORF g.25535 m.25535 type:internal len:362 (...  65.5    4e-09
lcl|m.108744  g.108744  ORF g.108744 m.108744 type:5prime_partial...  65.1    5e-09
lcl|m.93491  g.93491  ORF g.93491 m.93491 type:complete len:398 (...  64.3    8e-09
lcl|m.35605  g.35605  ORF g.35605 m.35605 type:3prime_partial len...  64.3    9e-09
lcl|m.48542  g.48542  ORF g.48542 m.48542 type:3prime_partial len...  63.9    1e-08
lcl|m.125362  g.125362  ORF g.125362 m.125362 type:5prime_partial...  63.5    1e-08
lcl|m.177235  g.177235  ORF g.177235 m.177235 type:internal len:2...  63.5    2e-08
lcl|m.36118  g.36118  ORF g.36118 m.36118 type:internal len:797 (...  63.2    2e-08
lcl|m.163124  g.163124  ORF g.163124 m.163124 type:complete len:8...  63.2    2e-08
lcl|m.181499  g.181499  ORF g.181499 m.181499 type:5prime_partial...  63.2    2e-08
lcl|m.79942  g.79942  ORF g.79942 m.79942 type:5prime_partial len...  62.8    2e-08
lcl|m.301305  g.301305  ORF g.301305 m.301305 type:internal len:7...  62.8    2e-08
lcl|m.9377  g.9377  ORF g.9377 m.9377 type:internal len:451 (-) c...  62.8    3e-08
lcl|m.447055  g.447055  ORF g.447055 m.447055 type:complete len:5...  62.8    3e-08
lcl|m.181564  g.181564  ORF g.181564 m.181564 type:5prime_partial...  62.0    4e-08
lcl|m.447076  g.447076  ORF g.447076 m.447076 type:5prime_partial...  62.0    4e-08
lcl|m.490491  g.490491  ORF g.490491 m.490491 type:5prime_partial...  62.0    4e-08
lcl|m.43643  g.43643  ORF g.43643 m.43643 type:3prime_partial len...  61.6    5e-08
lcl|m.155551  g.155551  ORF g.155551 m.155551 type:5prime_partial...  61.6    5e-08
lcl|m.259610  g.259610  ORF g.259610 m.259610 type:5prime_partial...  61.6    6e-08
lcl|m.118462  g.118462  ORF g.118462 m.118462 type:5prime_partial...  61.2    8e-08
lcl|m.7342  g.7342  ORF g.7342 m.7342 type:internal len:404 (+) c...  61.2    8e-08
lcl|m.123041  g.123041  ORF g.123041 m.123041 type:complete len:1...  60.8    9e-08
lcl|m.142598  g.142598  ORF g.142598 m.142598 type:complete len:3...  60.8    9e-08
lcl|m.54189  g.54189  ORF g.54189 m.54189 type:internal len:103 (...  60.8    9e-08
lcl|m.98532  g.98532  ORF g.98532 m.98532 type:internal len:494 (...  60.8    1e-07
lcl|m.490561  g.490561  ORF g.490561 m.490561 type:5prime_partial...  60.5    1e-07
lcl|m.90943  g.90943  ORF g.90943 m.90943 type:complete len:727 (...  60.5    1e-07
lcl|m.183210  g.183210  ORF g.183210 m.183210 type:complete len:1...  60.5    1e-07
lcl|m.446984  g.446984  ORF g.446984 m.446984 type:complete len:3...  60.1    2e-07
lcl|m.36188  g.36188  ORF g.36188 m.36188 type:5prime_partial len...  60.1    2e-07
lcl|m.63945  g.63945  ORF g.63945 m.63945 type:5prime_partial len...  59.7    2e-07
lcl|m.111709  g.111709  ORF g.111709 m.111709 type:5prime_partial...  59.3    2e-07
lcl|m.75361  g.75361  ORF g.75361 m.75361 type:internal len:1068 ...  59.3    3e-07
lcl|m.159987  g.159987  ORF g.159987 m.159987 type:5prime_partial...  58.5    4e-07
lcl|m.166015  g.166015  ORF g.166015 m.166015 type:5prime_partial...  57.4    9e-07
lcl|m.226081  g.226081  ORF g.226081 m.226081 type:complete len:1...  57.4    1e-06
lcl|m.110529  g.110529  ORF g.110529 m.110529 type:complete len:8...  57.0    1e-06
lcl|m.1144371  g.1144371  ORF g.1144371 m.1144371 type:complete l...  57.0    2e-06
lcl|m.84030  g.84030  ORF g.84030 m.84030 type:5prime_partial len...  56.6    2e-06
lcl|m.490518  g.490518  ORF g.490518 m.490518 type:5prime_partial...  56.2    2e-06
lcl|m.447043  g.447043  ORF g.447043 m.447043 type:5prime_partial...  56.2    2e-06
lcl|m.277200  g.277200  ORF g.277200 m.277200 type:internal len:6...  56.2    2e-06
lcl|m.173445  g.173445  ORF g.173445 m.173445 type:5prime_partial...  55.8    3e-06
lcl|m.59716  g.59716  ORF g.59716 m.59716 type:5prime_partial len...  55.1    5e-06
lcl|m.159510  g.159510  ORF g.159510 m.159510 type:5prime_partial...  55.1    5e-06
lcl|m.270419  g.270419  ORF g.270419 m.270419 type:5prime_partial...  55.1    5e-06
lcl|m.490541  g.490541  ORF g.490541 m.490541 type:5prime_partial...  55.1    5e-06
lcl|m.342575  g.342575  ORF g.342575 m.342575 type:complete len:3...  53.9    1e-05
lcl|m.230988  g.230988  ORF g.230988 m.230988 type:internal len:1...  53.5    2e-05
lcl|m.170680  g.170680  ORF g.170680 m.170680 type:complete len:8...  52.0    4e-05
lcl|m.212398  g.212398  ORF g.212398 m.212398 type:internal len:1...  52.0    4e-05
lcl|m.95968  g.95968  ORF g.95968 m.95968 type:complete len:514 (...  52.0    5e-05
lcl|m.17401  g.17401  ORF g.17401 m.17401 type:complete len:184 (...  51.2    8e-05
lcl|m.252260  g.252260  ORF g.252260 m.252260 type:complete len:4...  49.7    2e-04
lcl|m.235161  g.235161  ORF g.235161 m.235161 type:complete len:2...  48.1    6e-04
lcl|m.153212  g.153212  ORF g.153212 m.153212 type:5prime_partial...  48.1    6e-04
lcl|m.30566  g.30566  ORF g.30566 m.30566 type:internal len:87 (+...  48.1    7e-04
lcl|m.237744  g.237744  ORF g.237744 m.237744 type:complete len:9...  46.6    0.002
lcl|m.906439  g.906439  ORF g.906439 m.906439 type:internal len:6...  35.8    3.3  
lcl|m.295575  g.295575  ORF g.295575 m.295575 type:internal len:1...  34.7    6.6  


>lcl|m.163238 g.163238  ORF g.163238 m.163238 type:5prime_partial len:450 (+) 
comp10307_c0_seq9:3-1352(+)
Length=450

 Score =  121 bits (303),  Expect = 6e-26, Method: Compositional matrix adjust.
 Identities = 94/298 (31%), Positives = 134/298 (44%), Gaps = 53/298 (17%)

Query  29   VPRPRHGHRAVAIKELIVVFGGGN-EGIVDELHVYNTATNQWFIPAVRGDIPPG----CA  83
            VP PR GH  +A+   IVVFGG + EG+VD+L  ++  T  W   A +G  P G    CA
Sbjct  61   VPSPRLGHSCIALDRKIVVFGGAHTEGVVDDLFCFDLETLSWSRVAAKGSPPCGRYDHCA  120

Query  84   AYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPRLGHSFS  143
             +  + +   +LVFGG    G    DLY L   + EW++L+A     G  P PR  H  +
Sbjct  121  VH--IPERREMLVFGGARPDGNL-QDLYALDLDKLEWRQLQA----TGETPSPRTLHHAA  173

Query  144  LVGNKCYLFGGLANDSEDP--KNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPPPRE  201
             +  K Y+FGG  +   DP   + +  Y  DL +           W   +T G  P  R 
Sbjct  174  AISQKLYVFGG-GHTGMDPVSDDKMHVYDRDLNV-----------WTQLVTKGKTPRMRH  221

Query  202  SHTAVVYTEKDNKKSKLVIYGGMSGCR-LGDLWTLDIDTLTWNKPSLSGVAPLPRSLHSA  260
             HTA V        + +   GGM+G   L D++  + +  TW  P  SG + LPRS H A
Sbjct  222  GHTATVV------GNSIYYIGGMAGGEFLNDVFVFNTEDKTWTYPKTSGASLLPRSGHGA  275

Query  261  TTIGNKMYVFGGW-----VPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWETILMDT  313
              +  +++VFGG       P   DD               +  L+L TM W  + + T
Sbjct  276  ALVKGRIFVFGGLGITPAGPSAFDD---------------MLALDLATMTWSHVDIST  318


 Score = 79.7 bits (195),  Expect = 2e-13, Method: Compositional matrix adjust.
 Identities = 67/236 (28%), Positives = 95/236 (40%), Gaps = 38/236 (16%)

Query  112  ELQASRWEWKRLKAKTPKNGPP-----PCPRLGHSFSLVGNKCYLFGGLANDSEDPKNNI  166
            EL+   W    + A +P  G       P PRLGHS   +  K  +FGG   +        
Sbjct  38   ELKPGVW---YVAAYSPDEGAAAHVFVPSPRLGHSCIALDRKIVVFGGAHTEG-------  87

Query  167  PRYLNDLYILELRPGSGVVAWDIPITYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSG  226
               ++DL+  +L      ++W      G  P  R  H AV   E+   +  LV  G    
Sbjct  88   --VVDDLFCFDLE----TLSWSRVAAKGSPPCGRYDHCAVHIPER---REMLVFGGARPD  138

Query  227  CRLGDLWTLDIDTLTWNKPSLSGVAPLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATH  286
              L DL+ LD+D L W +   +G  P PR+LH A  I  K+YVFGG     MD V     
Sbjct  139  GNLQDLYALDLDKLEWRQLQATGETPSPRTLHHAAAISQKLYVFGG-GHTGMDPVS----  193

Query  287  EKEWKCTNTLACLNLDTMAWETILMDTLEDNIPRARAGHCAVAINTRLYIWSGRDG  342
                   + +   + D   W  ++    +   PR R GH A  +   +Y   G  G
Sbjct  194  ------DDKMHVYDRDLNVWTQLVT---KGKTPRMRHGHTATVVGNSIYYIGGMAG  240


 Score = 67.4 bits (163),  Expect = 1e-09, Method: Compositional matrix adjust.
 Identities = 44/146 (30%), Positives = 68/146 (46%), Gaps = 10/146 (6%)

Query  14   LLQPRWKRVVGWSGPVPRPRHGHRAVAIKELIVVFGGGNEGI----VDELHVYNTATNQW  69
            L +  W+++   +G  P PR  H A AI + + VFGGG+ G+     D++HVY+   N W
Sbjct  149  LDKLEWRQLQA-TGETPSPRTLHHAAAISQKLYVFGGGHTGMDPVSDDKMHVYDRDLNVW  207

Query  70   FIPAVRGDIPPGCAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPK  129
                 +G  P     +     G  +   GGM   G++ ND++        W   K     
Sbjct  208  TQLVTKGKTPRMRHGHTATVVGNSIYYIGGMAG-GEFLNDVFVFNTEDKTWTYPKT----  262

Query  130  NGPPPCPRLGHSFSLVGNKCYLFGGL  155
            +G    PR GH  +LV  + ++FGGL
Sbjct  263  SGASLLPRSGHGAALVKGRIFVFGGL  288


>lcl|m.230095 g.230095  ORF g.230095 m.230095 type:5prime_partial len:427 (-) 
comp12008_c0_seq1:1078-2358(-)
Length=427

 Score =  118 bits (296),  Expect = 4e-25, Method: Compositional matrix adjust.
 Identities = 15/291 (5%), Positives = 41/291 (14%), Gaps = 40/291 (13%)

Query  30   PRPRHGHRAVAI------KELIVVFGGGN-EGIVDELHVYNTATNQWF-IPAVRGD-IPP  80
                   +A+A         L +   G                   +    +V G     
Sbjct  63   STGSTLEQALAAVNTYVGHLLALQSAGQAGAAQPGPALGRMQTAVTFKWADSVAGAINAF  122

Query  81   GCAAYGFV--CDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPRL  138
                           +             ++  +             +   +G       
Sbjct  123  TSVDEELANVLLAFAVWCTRRSAALPFS-SNGDDPATKEAYHC---LRRA-SGLFDYVVE  177

Query  139  GHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPP  198
                ++  +    F      S                          A        ++  
Sbjct  178  LRRRAVSHHPGTDFDERVLKSL------SAQALAEAQEITL----ERARQKGNAPDLIAQ  227

Query  199  PRESHTAVVYTEKDNKKSKLVIYGGMSGCRL-GDLWTLDIDTLTWNKPSLSGVAPLPRSL  257
                     +   +       I   ++  R             + +   +   A      
Sbjct  228  IAADTQ-AKFEAAEASV--KDIPAMLAVYRTYLAFKKSFYKAFSQHYQGMYLFAQDKCGD  284

Query  258  HSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCT-NTLACLNLDTMAWE  307
                    +  +                           L    + T    
Sbjct  285  AIKVFTLARESLDKA---AKDSQ------GFATAMRCRPLTQHTVYTNLDT  326


 Score = 83.2 bits (204),  Expect = 2e-14, Method: Compositional matrix adjust.
 Identities = 13/263 (4%), Positives = 30/263 (11%), Gaps = 49/263 (18%)

Query  18   RWKRVVGWSG-PVPRPRHGHRAVAIKE---LIVVFGGGNEGIVDELHVYNTATNQWFIPA  73
             +K     +G              +     +                   T      +  
Sbjct  108  TFKWADSVAGAINAFTSVDEELANVLLAFAVWCTRRSAALPFSSNGDDPATKEAYHCLRR  167

Query  74   VRGDIPPGCAAYGFVCDGTRLLVFGGMVEYGKYSND------LYELQASRWEWKRLKAKT  127
              G                    F          +         +        ++  A  
Sbjct  168  ASGLFDYVVELRRRAVSHHPGTDFDE----RVLKSLSAQALAEAQEITLERARQKGNAP-  222

Query  128  PKNGPPPCPRLGHSF-SLVGN--KCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGV  184
                                      +   LA                            
Sbjct  223  ---DLIAQIAADTQAKFEAAEASVKDIPAMLAVYR---------TYLAFKKSF----YKA  266

Query  185  VAWDIPITYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSG-------CRLGDLWTLDI  237
             +      Y            V         ++  +              R   L    +
Sbjct  267  FSQHYQGMYLFAQDKCGDAIKVFT------LARESLDKAAKDSQGFATAMRCRPLTQHTV  320

Query  238  DTLTWNKPSLSGVAPLPRSL-HS  259
             T        + +          
Sbjct  321  YTNLDTVLKKA-LEKANHENGFI  342


 Score = 65.5 bits (158),  Expect = 4e-09, Method: Compositional matrix adjust.
 Identities = 16/272 (5%), Positives = 36/272 (13%), Gaps = 53/272 (19%)

Query  81   GCAAYGFVCD---GTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPR  137
               A   V         L   G     +          +   +K   A +          
Sbjct  68   LEQALAAVNTYVGHLLALQSAGQAGAAQP-GPALGRMQTAVTFK--WADSVAGAINAFTS  124

Query  138  LGHSFSLVGN--KCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGV  195
            +    + V      +     A              ++      +         +    G+
Sbjct  125  VDEELANVLLAFAVWCTRRSAALPF---------SSNGDDPATKEAY----HCLRRASGL  171

Query  196  LPPPRESHTAVVYTEKDNKKSKLVIYGGMSGCR--LGD--LWTLDIDTLTWNKPSLSGVA  251
                 E     V             +                          K +   + 
Sbjct  172  FDYVVELRRRAVSHHP------GTDFDERVLKSLSAQALAEAQEITLERARQKGNAPDLI  225

Query  252  PLPRSLHSATTIGNK---MYVFGGWV-PLVMDDVKVATHEKEWKCTNTLACLNLDTMAWE  307
                +   A     +     +             K                      +  
Sbjct  226  AQIAADTQAKFEAAEASVKDIPAMLAVYRTYLAFKK---------------SFYKAFSQH  270

Query  308  TILMDTLEDNIPRARAGHCAVAINTRLYIWSG  339
                                V    R  +   
Sbjct  271  --YQGM-YLFAQDKCGDAIKVFTLARESLDKA  299


 Score = 37.0 bits (84),  Expect = 1.3, Method: Compositional matrix adjust.
 Identities = 9/151 (5%), Positives = 24/151 (15%), Gaps = 24/151 (15%)

Query  195  VLPPPRESHTAVVYTEKDNKKSKLVIYGGMSGCR-LGDLWTLDIDTLTWNK-PSLSGVAP  252
             L        A+            +   G +G    G         +T+    S++G A 
Sbjct  61   ALSTGSTLEQALAAVNTYVGHLLALQSAGQAGAAQPGPALGRMQTAVTFKWADSVAG-AI  119

Query  253  LP--RSLHSATTIGN--KMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWET  308
                        +     ++                +       ++              
Sbjct  120  NAFTSVDEELANVLLAFAVWCTRR------------SAAL--PFSSNGDDPATKEAYHCL  165

Query  309  ILMDTLEDNIPRARAGHCAVAINTRLYIWSG  339
                                  +     +  
Sbjct  166  ---RRASGLFDYVVELRRRAVSHHPGTDFDE  193


>lcl|m.275446 g.275446  ORF g.275446 m.275446 type:complete len:528 (+) comp15696_c2_seq1:140-1723(+)
Length=528

 Score =  108 bits (270),  Expect = 4e-22, Method: Compositional matrix adjust.
 Identities = 92/298 (30%), Positives = 131/298 (43%), Gaps = 51/298 (17%)

Query  18   RWKRVVGWSGPVPRPRHGH--RAVAIKELIVVFGGGNEGIVDEL-HV--YNTATNQWFIP  72
            RW +V   SG  P    GH    +  K +  +FGG ++    E  HV  +N   +QW   
Sbjct  59   RWAKVPC-SGQTPPRSEGHTLSFMETKGMFYLFGGADDNEDVEFNHVMTFNPKAHQWAPI  117

Query  73   AVRGDIPPGCAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGP  132
            AV+G  P     +     G++L VFGG V+ G  +NDL+   A+   W+ L A       
Sbjct  118  AVKGTKPAARLNHAAAVIGSKLYVFGGFVD-GVATNDLHCFDATSNTWQELNAANK----  172

Query  133  PPCPRLGHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPIT  192
             P  R  H+ + +  K Y+FGG     ED         ND+++ +  P +   AW    T
Sbjct  173  -PAARCDHTLNAINGKLYVFGGRG--GEDI------LFNDVHVFD--PSTN--AWAPVET  219

Query  193  YGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSGCR-------LGDLWTLDIDTLTWNKP  245
             G  P  R+ HTAV   ++      LV++GG              D+  L++D  TW   
Sbjct  220  TGTAPSKRDFHTAVTVGDQ------LVVFGGAYEIESQNVSENFNDVHVLNVDNGTWQAL  273

Query  246  SLSGVAPLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDT  303
            + SG  P  R  H+A   GNKMYVFGG            THE     TN L CL +++
Sbjct  274  APSGTVPSTRWQHAACAFGNKMYVFGG------------THED--IDTNDLLCLTVES  317


 Score = 80.9 bits (198),  Expect = 8e-14, Method: Compositional matrix adjust.
 Identities = 95/363 (26%), Positives = 139/363 (38%), Gaps = 111/363 (30%)

Query  79   PPGCAAYGFVC--DGTRLLVFGGMVE--YGKYS--NDLYELQAS---RWEWKRLKAKTPK  129
            P   +A+  VC  D T L +FGGMV    G+ S  ND+Y++      RW      AK P 
Sbjct  13   PRARSAHTLVCGADAT-LFLFGGMVSDAQGEISALNDMYQINGDNPTRW------AKVPC  65

Query  130  NGPPPCPRLGHSFSLVGNK--CYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAW  187
            +G  P    GH+ S +  K   YLFGG A+D+ED + N        +++   P +   A 
Sbjct  66   SGQTPPRSEGHTLSFMETKGMFYLFGG-ADDNEDVEFN--------HVMTFNPKAHQWA-  115

Query  188  DIPI-TYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGM-SGCRLGDLWTLDIDTLTW---  242
              PI   G  P  R +H A V        SKL ++GG   G    DL   D  + TW   
Sbjct  116  --PIAVKGTKPAARLNHAAAVIG------SKLYVFGGFVDGVATNDLHCFDATSNTWQEL  167

Query  243  ---NKPSL--------------------------------------------SGVAPLPR  255
               NKP+                                             +G AP  R
Sbjct  168  NAANKPAARCDHTLNAINGKLYVFGGRGGEDILFNDVHVFDPSTNAWAPVETTGTAPSKR  227

Query  256  SLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWETILMDTLE  315
              H+A T+G+++ VFGG   +   +V         +  N +  LN+D   W+ +      
Sbjct  228  DFHTAVTVGDQLVVFGGAYEIESQNVS--------ENFNDVHVLNVDNGTWQAL---APS  276

Query  316  DNIPRARAGHCAVAINTRLYIWSGRDGYRKAWNNQVCCKDLWYLETEKPP-----PPARV  370
              +P  R  H A A   ++Y++ G        +  +   DL  L  E  P     PP R 
Sbjct  277  GTVPSTRWQHAACAFGNKMYVFGGT-------HEDIDTNDLLCLTVESAPRAGVAPPVRR  329

Query  371  QLV  373
            + +
Sbjct  330  KTI  332


 Score = 75.5 bits (184),  Expect = 4e-12, Method: Compositional matrix adjust.
 Identities = 66/236 (27%), Positives = 100/236 (42%), Gaps = 40/236 (16%)

Query  119  EWKRLKAKTPKNGPPPCPRLGHSFSLVGNK-CYLFGGLANDSEDPKNNIPRYLNDLYILE  177
            EW  LKA +      P  R  H+     +   +LFGG+ +D++   +     LND+Y + 
Sbjct  3    EWATLKALSA-----PRARSAHTLVCGADATLFLFGGMVSDAQGEIS----ALNDMYQIN  53

Query  178  LRPGSGVVAW-DIPITYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSG---CRLGDLW  233
               G     W  +P + G  PP  E HT + + E    K    ++GG           + 
Sbjct  54   ---GDNPTRWAKVPCS-GQTPPRSEGHT-LSFMET---KGMFYLFGGADDNEDVEFNHVM  105

Query  234  TLDIDTLTWNKPSLSGVAPLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCT  293
            T +     W   ++ G  P  R  H+A  IG+K+YVFGG+V  V               T
Sbjct  106  TFNPKAHQWAPIAVKGTKPAARLNHAAAVIGSKLYVFGGFVDGV--------------AT  151

Query  294  NTLACLNLDTMAWETILMDTLEDNIPRARAGHCAVAINTRLYIWSGRDGYRKAWNN  349
            N L C +  +  W+ +       N P AR  H   AIN +LY++ GR G    +N+
Sbjct  152  NDLHCFDATSNTWQELNAA----NKPAARCDHTLNAINGKLYVFGGRGGEDILFND  203


>lcl|m.234512 g.234512  ORF g.234512 m.234512 type:complete len:612 (-) comp12669_c0_seq1:641-2476(-)
Length=612

 Score =  100 bits (249),  Expect = 1e-19, Method: Compositional matrix adjust.
 Identities = 18/335 (5%), Positives = 52/335 (15%), Gaps = 65/335 (19%)

Query  29   VPRPRHGHRAVAIK---ELIVVFGG------GNEGIVDELHVYNTATNQWFIPAVRGDIP  79
                             +  +          G +  +   H      ++  +        
Sbjct  28   ELAGVVLL-LPPQPCAPDKRLGAQAAAARNLGLDLALGLRHELLLVAHEVLVGHHHVHQA  86

Query  80   PG--CAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPR  137
                 A         R  +   ++                         +   G      
Sbjct  87   HRLLAALDIHHLACGRCQLLLLLLACRTR-GRCCGRGWGGGSSW----GSGGGGILQLLE  141

Query  138  LGHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLP  197
                 + +G        L +               L + +          D     G+  
Sbjct  142  GLGLLADLGGLTLQLLTLEHGCG---------DRLLALRK----HVRRRVDPL-PDGLDV  187

Query  198  PPRESHTAVVYTEKDNKKSKLVIYGGMSG--CRLGDLWTLDIDTLTWNKPSLSGV-APLP  254
               +++  V                 +      L  +           + +         
Sbjct  188  EAVQANAEVEDAL------ASHRARALERECLGLLVVDLNAKLLFREGQLAALSPGERDD  241

Query  255  RSLHSATTIG----NKMYVFGGWVPLVMDDVKVATHEKE----WKCTNTLACLNLDTMAW  306
            R++  A                                          T   L+   +  
Sbjct  242  RAVQLAVAEDEQAAWVHAEHKR------------RALVVLDLEVLHCETAVLLDDLDVLE  289

Query  307  ETILMDTLEDNIPRARAGHCAV--AINTRLYIWSG  339
            +       +D + R       +     +   +   
Sbjct  290  DRQ---VAQDALQRLPDADHLLGPHQPSVCGLDRR  321


 Score = 90.1 bits (222),  Expect = 1e-16, Method: Compositional matrix adjust.
 Identities = 16/266 (6%), Positives = 38/266 (14%), Gaps = 35/266 (13%)

Query  21   RVVGWSGPVPRPRHGHRAVAIKEL--IVVFGGG-NEGIVDELHVYNTATNQWFIPAVRGD  77
             V                +         +                       +     G 
Sbjct  77   LVGHHHVHQAHRLLAALDIHHLACGRCQLLLLLLACRTRGRCCGRGWGGGSSWGSGGGGI  136

Query  78   IP-PGCAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCP  136
            +             G   L    +   G     L   +  R            +G     
Sbjct  137  LQLLEGLGLLADLGGL-TLQLLTLEH-GCGDRLLALRKHVRRRVD-----PLPDGLDVEA  189

Query  137  RLGHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGV-  195
               ++        +    L  +       +   L  + +                     
Sbjct  190  VQANAEVEDALASHRARALERE------CL--GLLVVDLNAKLLF----REGQLAALSPG  237

Query  196  LPPPRESHTAVVYTEKDNKKSKLVIYGGMSGCR-------LGDLWTLDIDTLTWNKPSLS  248
                R    AV   E+         +   +                LD   +  ++    
Sbjct  238  ERDDRAVQLAVAEDEQAAW--VHAEHKRRALVVLDLEVLHCETAVLLDDLDVLEDRQVAQ  295

Query  249  GVAPLPRSLHSATT--IGNKMYVFGG  272
                              +   +   
Sbjct  296  DALQRLPDADHLLGPHQPSVCGLDRR  321


 Score = 78.2 bits (191),  Expect = 5e-13, Method: Compositional matrix adjust.
 Identities = 14/232 (6%), Positives = 37/232 (15%), Gaps = 39/232 (16%)

Query  134  PCPRLGHSFSL--VGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPI  191
                +          +K       A         +   L   + L L          +  
Sbjct  29   LAGVVLLLPPQPCAPDKRLGAQAAAAR----NLGLDLALGLRHELLL----VAHEVLVGH  80

Query  192  TYGVLPPPRESHTAVVYTEKDNKKSKLVIYGG-MSGCRLGDLWTLDIDTLTWNKPSLSGV  250
             +        +    ++     +     +    ++    G          +       G+
Sbjct  81   HHVHQAHRLLAAL-DIHHLACGR---CQLLLLLLACRTRGRCCGRGWGGGSSWGSGGGGI  136

Query  251  APLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWETIL  310
              L   L     +G                               LA         +   
Sbjct  137  LQLLEGLGLLADLGGLTLQLLTL----EHGCGDR----------LLALRKHVRRRVDP--  180

Query  311  MDTLEDNIPRARAGHCAVAINTRLYIWSGRDGYRKAWNNQVCCKDLWYLETE  362
                +     A   +  V      +     +         +    +      
Sbjct  181  --LPDGLDVEAVQANAEVEDALASHRARALER------ECLGLLVVDLNAKL  224


 Score = 62.8 bits (151),  Expect = 2e-08, Method: Compositional matrix adjust.
 Identities = 11/178 (6%), Positives = 23/178 (12%), Gaps = 33/178 (18%)

Query  25   WSGP----VPRPRHGHRAVAIKELIVVFGG--GNEGIVDELHVYNTATNQWFIPAVRGDI  78
                           +  V                  +  + +      +    A     
Sbjct  178  VDPLPDGLDVEAVQANAEVEDALASHRARALERECLGLLVVDLNAKLLFREGQLAALSPG  237

Query  79   PPGCAAYG-FVCDG----TRLLVFGGMVEY-----GKYSNDLYELQASRWEWKRLKAKTP  128
                 A    V +                        +      L        R      
Sbjct  238  ERDDRAVQLAVAEDEQAAWVHAEHKRRALVVLDLEVLHCETAVLLDDLDVLEDRQV---A  294

Query  129  KNGPPPCPRLGHSF--SLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILEL--RPGS  182
            ++                  + C L   +              L       L  + G 
Sbjct  295  QDALQRL-PDADHLLGPHQPSVCGLDRRVGQQLA---------LVAHGDQLLLGQRGC  342


>lcl|m.136234 g.136234  ORF g.136234 m.136234 type:5prime_partial len:533 (+) 
comp13135_c1_seq1:2-1600(+)
Length=533

 Score = 99.4 bits (246),  Expect = 2e-19, Method: Composition-based stats.
 Identities = 78/256 (30%), Positives = 111/256 (43%), Gaps = 33/256 (12%)

Query  26   SGPVPRPRHGHRAVAIKELIVVFGGGNEGIVDELH-VYNTATN-QWFIPAVRGDIPPGCA  83
            SGP  R  H    V   +L V FGG ++    + + VY  +++ +W      G+ P    
Sbjct  83   SGPSKREGHTLTYVKDSDLFVCFGGSDDVTETDFNDVYILSSDLEWKKMKTTGNGPAPRL  142

Query  84   AYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPRLGHSFS  143
             +     G  L VFGG V+ G   NDLY+L  + ++W  +K         P  R  HS +
Sbjct  143  NHASAVVGNDLYVFGGFVD-GDAKNDLYKLDTTSFQWSLIKV-----AGAPARRCNHSLT  196

Query  144  LVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPPPRESH  203
             VG+K Y+FGG   ++           ND+   +    +    W      G  P  R+ H
Sbjct  197  SVGSKLYVFGGRGGEAT--------LYNDISCFDTESHT----WTTLTPSGPQPSERDFH  244

Query  204  TAVVYTEKDNKKSKLVIYGGMSGCR-------LGDLWTLDIDTLTWNKPSLSGVAPLPRS  256
            TA  Y +K      + I+GG              D++  D     W KP++SG AP  R 
Sbjct  245  TAATYGDK------IYIFGGAYEIESKDIFRYFNDVFAFDTSRGVWVKPNISGTAPRVRW  298

Query  257  LHSATTIGNKMYVFGG  272
             HSA   GNKMYVFGG
Sbjct  299  AHSACVSGNKMYVFGG  314


 Score = 96.7 bits (239),  Expect = 2e-18, Method: Composition-based stats.
 Identities = 99/341 (29%), Positives = 147/341 (43%), Gaps = 50/341 (14%)

Query  12   AVLLQPRWKRVVGWSGPVPRPRHGHRA-VAIKELIVVFGG------GNEGIVDELHVYNT  64
            AV+L   WK V G  G  P  R  H   V+ +  IV+FGG      G+    D+  V N 
Sbjct  11   AVMLSAEWKVVRG-VGTAPPIRSTHDMDVSNQGKIVIFGGMTTNDEGDPIPYDDTFVLNV  69

Query  65   -ATNQWFIPAVRGDIPPGCAAYG--FVCDGTRLLVFGGMVEYGKYS-NDLYELQASRWEW  120
                 W   A  G  P     +   +V D    + FGG  +  +   ND+Y L +S  EW
Sbjct  70   DGKAVWGSNATHGSGPSKREGHTLTYVKDSDLFVCFGGSDDVTETDFNDVYIL-SSDLEW  128

Query  121  KRLKAKTPKNGPPPCPRLGHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRP  180
            K++K  T  NGP   PRL H+ ++VGN  Y+FGG  +   D KN       DLY L+   
Sbjct  129  KKMK--TTGNGP--APRLNHASAVVGNDLYVFGGFVDG--DAKN-------DLYKLD---  172

Query  181  GSGVVAWDIPITYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSG--CRLGDLWTLDID  238
             +    W +    G  P  R +H+        +  SKL ++GG  G      D+   D +
Sbjct  173  -TTSFQWSLIKVAGA-PARRCNHSLT------SVGSKLYVFGGRGGEATLYNDISCFDTE  224

Query  239  TLTWNKPSLSGVAPLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLAC  298
            + TW   + SG  P  R  H+A T G+K+Y+FGG   +   D+        ++  N +  
Sbjct  225  SHTWTTLTPSGPQPSERDFHTAATYGDKIYIFGGAYEIESKDI--------FRYFNDVFA  276

Query  299  LNLDTMAWETILMDTLEDNIPRARAGHCAVAINTRLYIWSG  339
             +     W   +   +    PR R  H A     ++Y++ G
Sbjct  277  FDTSRGVW---VKPNISGTAPRVRWAHSACVSGNKMYVFGG  314


>lcl|m.136096 g.136096  ORF g.136096 m.136096 type:complete len:954 (+) comp14883_c8_seq2:2459-5320(+)
Length=954

 Score = 99.4 bits (246),  Expect = 3e-19, Method: Compositional matrix adjust.
 Identities = 84/293 (28%), Positives = 126/293 (43%), Gaps = 52/293 (17%)

Query  27   GPVPRPRHGHRAVAIKELIVVFGGGNEGIVDELHVYNTATNQWFIPAVRGDIPPGCAAY-  85
            GP PR  H    V+   L++  G   E  ++++HV++  + +W  P   G  P     Y 
Sbjct  47   GPAPRLGHSCCLVSPTNLLLFAGATLEAPLNDIHVFDLESFEWSKPECTGKAPA--LRYD  104

Query  86   ---GFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPK-NGPPPCPRLGHS  141
               GF+    +L+VFGG  +  +   D++ L  + W W      +PK +G  P PR  H+
Sbjct  105  HGAGFIPLLNKLVVFGG-AQPERNLCDIHLLDTTTWTWS-----SPKVSGSAPSPRTAHT  158

Query  142  FSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPPPRE  201
              ++     +FGG      DP +      +D+Y  ++       AW      G +P PR 
Sbjct  159  VGVIDAHLVVFGG-GKQGMDPVDT-----SDVYFFDVV----TCAWKRIKGTGSVPTPRH  208

Query  202  SHTAVVYTEKDNKKSKLVIYGGMSGC-RLGDLWTLDIDTLTWNKPSLSGVAPLPRSLHSA  260
             HT  V   +      L ++GGM+G    GDL+  D+DT  W  P   G AP  RS H+A
Sbjct  209  GHTMTVVGRR------LFVFGGMAGSTFFGDLYIFDLDTHVWTCPDTGGRAPSARSGHAA  262

Query  261  TTIG--NKMYVFGGW-----VPLVMDDVKVATHEKEWKCTNTLACLNLDTMAW  306
                  N++ VFGG       P  + DV V               L LDT+ W
Sbjct  263  ALDAEHNRLIVFGGLGLVSGAPTALADVHV---------------LALDTLVW  300


 Score = 72.8 bits (177),  Expect = 3e-11, Method: Compositional matrix adjust.
 Identities = 65/211 (30%), Positives = 91/211 (43%), Gaps = 33/211 (15%)

Query  134  PCPRLGHSFSLVG-NKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPIT  192
            P PRLGHS  LV      LF G   ++          LND+++ +L        W  P  
Sbjct  48   PAPRLGHSCCLVSPTNLLLFAGATLEAP---------LNDIHVFDLES----FEWSKPEC  94

Query  193  YGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSGCR-LGDLWTLDIDTLTWNKPSLSGVA  251
             G  P  R  H A      +    KLV++GG    R L D+  LD  T TW+ P +SG A
Sbjct  95   TGKAPALRYDHGAGFIPLLN----KLVVFGGAQPERNLCDIHLLDTTTWTWSSPKVSGSA  150

Query  252  PLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWETILM  311
            P PR+ H+   I   + VFGG     MD V           T+ +   ++ T AW+ I  
Sbjct  151  PSPRTAHTVGVIDAHLVVFGG-GKQGMDPVD----------TSDVYFFDVVTCAWKRI--  197

Query  312  DTLEDNIPRARAGHCAVAINTRLYIWSGRDG  342
                 ++P  R GH    +  RL+++ G  G
Sbjct  198  -KGTGSVPTPRHGHTMTVVGRRLFVFGGMAG  227


 Score = 54.7 bits (130),  Expect = 8e-06, Method: Compositional matrix adjust.
 Identities = 50/160 (31%), Positives = 68/160 (42%), Gaps = 18/160 (11%)

Query  26   SGPVPRPRHGHRAVAIKELIVVFGGGNEGI--VD--ELHVYNTATNQWFIPAVRGDIPPG  81
            SG  P PR  H    I   +VVFGGG +G+  VD  +++ ++  T  W      G +P  
Sbjct  147  SGSAPSPRTAHTVGVIDAHLVVFGGGKQGMDPVDTSDVYFFDVVTCAWKRIKGTGSVPTP  206

Query  82   CAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPK-NGPPPCPRLGH  140
               +     G RL VFGGM     +  DLY        W       P   G  P  R GH
Sbjct  207  RHGHTMTVVGRRLFVFGGMAG-STFFGDLYIFDLDTHVW-----TCPDTGGRAPSARSGH  260

Query  141  SFSLVG--NKCYLFGGLANDSEDPKNNIPRYLNDLYILEL  178
            + +L    N+  +FGGL   S       P  L D+++L L
Sbjct  261  AAALDAEHNRLIVFGGLGLVS-----GAPTALADVHVLAL  295


>lcl|m.47255 g.47255  ORF g.47255 m.47255 type:complete len:522 (-) comp7320_c0_seq1:258-1823(-)
Length=522

 Score = 98.2 bits (243),  Expect = 5e-19, Method: Composition-based stats.
 Identities = 77/259 (29%), Positives = 114/259 (44%), Gaps = 37/259 (14%)

Query  26   SGPVPRPRHGHRAVAIKE--LIVVFGGGN---EGIVDELHVYNTATNQWFIPAVRGDIPP  80
            SG  P  R GH    I +    + FGG +   E   ++++V +++  +W      G+ P 
Sbjct  69   SGSGPSKREGHTITYIPDGDFFICFGGSDDITEQDFNDVYVLSSSL-EWKKMITSGNGPA  127

Query  81   GCAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPRLGH  140
                +     G+ L VFGG V+ G   NDLY+L  + ++W  +K         P  R  H
Sbjct  128  PRLNHAATAVGSDLYVFGGFVD-GDAKNDLYKLDTTTFQWSLIKVANA-----PSRRCDH  181

Query  141  SFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPPPR  200
            + + VG   Y+FGG   ++           ND+   +    +    W      G LPP R
Sbjct  182  TITAVGTNLYVFGGRGGEAT--------LYNDITCFDTLNHT----WTTLEPSGQLPPER  229

Query  201  ESHTAVVYTEKDNKKSKLVIYGGMSGCR-------LGDLWTLDIDTLTWNKPSLSGVAPL  253
            + H+AV + +K      + I+GG              D++  D     W KPS+SG AP 
Sbjct  230  DFHSAVAFNDK------IFIFGGAYEIESKDIFKYFNDVFAFDTQRGVWVKPSVSGTAPT  283

Query  254  PRSLHSATTIGNKMYVFGG  272
             R  HSA   GNKMYVFGG
Sbjct  284  VRWAHSACVFGNKMYVFGG  302


 Score = 78.6 bits (192),  Expect = 4e-13, Method: Composition-based stats.
 Identities = 75/260 (28%), Positives = 107/260 (41%), Gaps = 44/260 (16%)

Query  119  EWKRLKAKTPKNGPPPCPRLGHSFSLV-GNKCYLFGGL-ANDSEDPKNNIPRYLNDLYIL  176
            EW  ++A     G  P  R  H   +    K  +FGG+ AN   DP   +P   +D ++L
Sbjct  5    EWATVRAV----GTGPAIRSTHDMDVSEAGKIVIFGGMTANKEGDP---VP--YDDTFVL  55

Query  177  ELRPGSGVVAWDIPITYGVLPPPRESHTAVVYTEKDNKKSKLVIYGG---MSGCRLGDLW  233
             +    G   W    T G  P  RE HT     + D      + +GG   ++     D++
Sbjct  56   NV---DGKAVWGSNATSGSGPSKREGHTITYIPDGDF----FICFGGSDDITEQDFNDVY  108

Query  234  TLDIDTLTWNKPSLSGVAPLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCT  293
             L   +L W K   SG  P PR  H+AT +G+ +YVFGG+V              +    
Sbjct  109  VLS-SSLEWKKMITSGNGPAPRLNHAATAVGSDLYVFGGFV--------------DGDAK  153

Query  294  NTLACLNLDTMAWETILMDTLEDNIPRARAGHCAVAINTRLYIWSGRDGYRKAWNNQVCC  353
            N L  L+  T  W  I +     N P  R  H   A+ T LY++ GR G    +N+  C 
Sbjct  154  NDLYKLDTTTFQWSLIKV----ANAPSRRCDHTITAVGTNLYVFGGRGGEATLYNDITCF  209

Query  354  KDLWYLETEKPP----PPAR  369
              L +  T   P    PP R
Sbjct  210  DTLNHTWTTLEPSGQLPPER  229


 Score = 77.0 bits (188),  Expect = 1e-12, Method: Composition-based stats.
 Identities = 92/340 (27%), Positives = 137/340 (40%), Gaps = 52/340 (15%)

Query  14   LLQPRWK--RVVGWSGPVPRPRHGHRAVAIKELIVVFGG------GNEGIVDELHVYNT-  64
            +L+  W   R VG +GP  R  H    V+    IV+FGG      G+    D+  V N  
Sbjct  1    MLKGEWATVRAVG-TGPAIRSTH-DMDVSEAGKIVIFGGMTANKEGDPVPYDDTFVLNVD  58

Query  65   ATNQWFIPAVRGDIPPGCAAYG--FVCDGTRLLVFGGMVEYGKYS-NDLYELQASRWEWK  121
                W   A  G  P     +   ++ DG   + FGG  +  +   ND+Y L +S  EWK
Sbjct  59   GKAVWGSNATSGSGPSKREGHTITYIPDGDFFICFGGSDDITEQDFNDVYVLSSS-LEWK  117

Query  122  RLKAKTPKNGPPPCPRLGHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPG  181
            ++   T  NGP   PRL H+ + VG+  Y+FGG  +   D KN       DLY L+    
Sbjct  118  KMI--TSGNGP--APRLNHAATAVGSDLYVFGGFVDG--DAKN-------DLYKLD----  160

Query  182  SGVVAWDIPITYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSG--CRLGDLWTLDIDT  239
            +    W + I     P  R  HT           + L ++GG  G      D+   D   
Sbjct  161  TTTFQWSL-IKVANAPSRRCDHTITAV------GTNLYVFGGRGGEATLYNDITCFDTLN  213

Query  240  LTWNKPSLSGVAPLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACL  299
             TW     SG  P  R  HSA    +K+++FGG   +   D+        +K  N +   
Sbjct  214  HTWTTLEPSGQLPPERDFHSAVAFNDKIFIFGGAYEIESKDI--------FKYFNDVFAF  265

Query  300  NLDTMAWETILMDTLEDNIPRARAGHCAVAINTRLYIWSG  339
            +     W   +  ++    P  R  H A     ++Y++ G
Sbjct  266  DTQRGVW---VKPSVSGTAPTVRWAHSACVFGNKMYVFGG  302


 Score = 63.5 bits (153),  Expect = 1e-08, Method: Composition-based stats.
 Identities = 50/178 (28%), Positives = 73/178 (41%), Gaps = 32/178 (17%)

Query  30   PRPRHGHRAVAIKELIVVFGG--GNEGIVDELHVYNTATNQWFIPAVRGDIPPGCAAYGF  87
            P  R  H   A+   + VFGG  G   + +++  ++T  + W      G +PP    +  
Sbjct  175  PSRRCDHTITAVGTNLYVFGGRGGEATLYNDITCFDTLNHTWTTLEPSGQLPPERDFHSA  234

Query  88   VCDGTRLLVFGGMVEYG-----KYSNDLYELQASRWEWKRLKAKTPKNGPPPCPRLGHSF  142
            V    ++ +FGG  E       KY ND++     R  W     K   +G  P  R  HS 
Sbjct  235  VAFNDKIFIFGGAYEIESKDIFKYFNDVFAFDTQRGVW----VKPSVSGTAPTVRWAHSA  290

Query  143  SLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPPPR  200
             + GNK Y+FGG AND +         LND + L +  G            G+ P PR
Sbjct  291  CVFGNKMYVFGGTANDED---------LNDCHALTIIDG------------GMKPAPR  327


>lcl|m.234536 g.234536  ORF g.234536 m.234536 type:5prime_partial len:562 (+) 
comp18916_c3_seq6:3-1688(+)
Length=562

 Score = 97.4 bits (241),  Expect = 8e-19, Method: Compositional matrix adjust.
 Identities = 90/331 (27%), Positives = 144/331 (43%), Gaps = 57/331 (17%)

Query  29   VPRPRHGHRAVAIK---ELIVVFGG------GNEGIVDELHVYNTATNQWFIPAVRGDIP  79
            +P PR GH A+A+      I+VFGG      G   +++++  +N AT  W     +GDIP
Sbjct  28   LPMPRSGH-AMAMNFATNSIIVFGGVGSNKQGEAEMLNDMFQFNVATRSWRPVRQQGDIP  86

Query  80   PG--CAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPR  137
                 A   +V     L++FGG VE  +Y ND Y    S   WK+        G PP PR
Sbjct  87   SKREGATLTYVESLNVLVLFGGGVEEAEY-NDTYTFDLSTATWKKRTC----TGQPPSPR  141

Query  138  LGHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLP  197
            L H+ +++    Y+FGG  +            + D++ LE    +   +W  P + G  P
Sbjct  142  LNHTAAIIDGALYIFGGFHDGVS---------MGDIFKLE----TDSFSWSSP-SRGDAP  187

Query  198  PPRESHTAVVYTEKDNKKSKLVIYGGMSG--CRLGDLWTLDIDTLTWNKPSLSGV-APLP  254
            P R +H+     +      KL ++GG +G      DL   D +T  W+ P + G  +P  
Sbjct  188  PARCNHSCTAVGK------KLFVFGGRAGENILFDDLQIYDTETNAWSTPHVRGKGSPTK  241

Query  255  RSLHSATTIG----NKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWETIL  310
            R  H+    G      + +FGG   L    V            N +  L++DT+ W  + 
Sbjct  242  RDFHTMVVAGPPSARTLCLFGGAHELESPQVS--------DIYNDVWLLDIDTVTWTHL-  292

Query  311  MDTLEDNIPRARAGHCAV--AINTRLYIWSG  339
                +  +P  R  H AV  A  ++++++ G
Sbjct  293  --QPKGGMPPTRWSHAAVSDATGSKMFVFGG  321


 Score = 87.4 bits (215),  Expect = 9e-16, Method: Compositional matrix adjust.
 Identities = 78/260 (30%), Positives = 114/260 (43%), Gaps = 35/260 (13%)

Query  27   GPVPRPRHGHRAVAIKEL--IVVFGGG-NEGIVDELHVYNTATNQWFIPAVRGDIP-PGC  82
            G +P  R G     ++ L  +V+FGGG  E   ++ + ++ +T  W      G  P P  
Sbjct  83   GDIPSKREGATLTYVESLNVLVLFGGGVEEAEYNDTYTFDLSTATWKKRTCTGQPPSPRL  142

Query  83   AAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPRLGHSF  142
                 + DG  L +FGG  + G    D+++L+   + W      +P  G  P  R  HS 
Sbjct  143  NHTAAIIDGA-LYIFGGFHD-GVSMGDIFKLETDSFSWS-----SPSRGDAPPARCNHSC  195

Query  143  SLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGV-LPPPRE  201
            + VG K ++FGG A +      NI    +DL I +        AW  P   G   P  R+
Sbjct  196  TAVGKKLFVFGGRAGE------NI--LFDDLQIYDTE----TNAWSTPHVRGKGSPTKRD  243

Query  202  SHTAVVYTEKDNKKSKLVIYGGMSGCR-------LGDLWTLDIDTLTWNKPSLSGVAPLP  254
             HT VV      +   L ++GG              D+W LDIDT+TW      G  P  
Sbjct  244  FHTMVVAGPPSAR--TLCLFGGAHELESPQVSDIYNDVWLLDIDTVTWTHLQPKGGMPPT  301

Query  255  RSLHSATT--IGNKMYVFGG  272
            R  H+A +   G+KM+VFGG
Sbjct  302  RWSHAAVSDATGSKMFVFGG  321


 Score = 76.6 bits (187),  Expect = 2e-12, Method: Compositional matrix adjust.
 Identities = 62/232 (26%), Positives = 98/232 (42%), Gaps = 39/232 (16%)

Query  134  PCPRLGHSFSL--VGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPI  191
            P PR GH+ ++    N   +FGG+ ++    K      LND++   +       +W    
Sbjct  29   PMPRSGHAMAMNFATNSIIVFGGVGSN----KQGEAEMLNDMFQFNV----ATRSWRPVR  80

Query  192  TYGVLPPPRESHTAVVYTEKDNKKSKLVIYGG-MSGCRLGDLWTLDIDTLTWNKPSLSGV  250
              G +P  RE  T + Y E  N    LV++GG +      D +T D+ T TW K + +G 
Sbjct  81   QQGDIPSKREGAT-LTYVESLNV---LVLFGGGVEEAEYNDTYTFDLSTATWKKRTCTGQ  136

Query  251  APLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWETIL  310
             P PR  H+A  I   +Y+FGG      D V +            +  L  D+ +W +  
Sbjct  137  PPSPRLNHTAAIIDGALYIFGG----FHDGVSMG----------DIFKLETDSFSWSS--  180

Query  311  MDTLEDNIPRARAGHCAVAINTRLYIWSGRDGYRKAWNNQVCCKDLWYLETE  362
                  + P AR  H   A+  +L+++ GR G        +   DL   +TE
Sbjct  181  --PSRGDAPPARCNHSCTAVGKKLFVFGGRAG------ENILFDDLQIYDTE  224


 Score = 61.6 bits (148),  Expect = 5e-08, Method: Compositional matrix adjust.
 Identities = 54/172 (31%), Positives = 74/172 (43%), Gaps = 31/172 (18%)

Query  25   WSGP----VPRPRHGHRAVAIKELIVVFGG--GNEGIVDELHVYNTATNQWFIPAVRGDI  78
            WS P     P  R  H   A+ + + VFGG  G   + D+L +Y+T TN W  P VRG  
Sbjct  178  WSSPSRGDAPPARCNHSCTAVGKKLFVFGGRAGENILFDDLQIYDTETNAWSTPHVRGKG  237

Query  79   PPGCAAYG-FVCDG----TRLLVFGGMVEY-----GKYSNDLYELQASRWEWKRLKAKTP  128
             P    +   V  G      L +FGG  E          ND++ L      W  L+   P
Sbjct  238  SPTKRDFHTMVVAGPPSARTLCLFGGAHELESPQVSDIYNDVWLLDIDTVTWTHLQ---P  294

Query  129  KNGPPPCPRLGHSF--SLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILEL  178
            K G PP  R  H+      G+K ++FGG    +E         LNDL++L+ 
Sbjct  295  KGGMPPT-RWSHAAVSDATGSKMFVFGGTGAVAE---------LNDLHVLDF  336


>lcl|m.19860 g.19860  ORF g.19860 m.19860 type:complete len:347 (-) comp5179_c0_seq1:109-1149(-)
Length=347

 Score = 96.7 bits (239),  Expect = 1e-18, Method: Compositional matrix adjust.
 Identities = 17/280 (6%), Positives = 44/280 (15%), Gaps = 46/280 (16%)

Query  42   KELIVVFGGGNEGIVDELHVYNTATNQWFIPAVRGDIPPGCAAYGFVCDGT-RLLVFGGM  100
            K+L                     +     P                   T ++      
Sbjct  58   KKLEEATSLETAKKQMASSSEQLLSRYPLHPEHTYAPNDRLERAIKGALDTFKMGGVLV-  116

Query  101  VEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPRLGHSFSLV--GNKCYLFGGLAND  158
            +   ++S     L+     +++ K            +L  +      G            
Sbjct  117  LHAPQHSGKSAALREVVRGYQKTKRF----NAILLRKLDITKEDGITGEFTTALQDA--C  170

Query  159  SEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPPPRESHTAVVYTEKDNKKSKL  218
                 +      +                + P    +L    +              ++L
Sbjct  171  GVPHDSKATWLPDF-------FELPP--NNEPKNIIILDQFDDDLPYTA------SATEL  215

Query  219  VIYGGM-SGCRLGDLWTLDIDTLTWNKPSLSGVAPLPRSLHSATTIGNKMYVFGGW----  273
              +    +     +   + I                   + S          F       
Sbjct  216  RGFVTSLATTAQANKLIVLIGVSDDAYARTMLACNGGTKIRSIWRNIKDGKNFKWLEEEL  275

Query  274  -VPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWETILMD  312
               L                      +    +  E     
Sbjct  276  VKVLDKHY---------------QEDILFHELVDEKARCK  300


 Score = 86.7 bits (213),  Expect = 1e-15, Method: Compositional matrix adjust.
 Identities = 10/230 (4%), Positives = 29/230 (12%), Gaps = 27/230 (11%)

Query  27   GPVPRPRHGHRAV-AIKELIVVFGGGN-EGIVDELHVYNTATNQWFIPAVRGDIPPGCAA  84
                               +      +      +          +        I      
Sbjct  92   YAPNDRLERAIKGALDTFKMGGVLVLHAPQHSGKSAALREVVRGYQKTKRFNAILLRKLD  151

Query  85   YGFVCDGT-RLLVFGGMVEYGKYSND----LYELQASRWEWKRLKAKTPKNGPPPCPRLG  139
                   T              + +         +           +             
Sbjct  152  ITKEDGITGEFTTALQDACGVPHDSKATWLPDFFELPPN------NEPKNIIILDQFDDD  205

Query  140  HSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPPP  199
              ++    +   F      +            +  I+ +    GV       T       
Sbjct  206  LPYTASATELRGFVTSLATT---------AQANKLIVLI----GVSDDAYARTMLACNGG  252

Query  200  RESHTAVVYTEKDNKKSKLVIYGGMSGCRLGDLWTLDIDTLTWNKPSLSG  249
             +  +     +       L                +    L   K     
Sbjct  253  TKIRSIWRNIKDGKNFKWLEEELVKV-LDKHYQEDILFHELVDEKARCKE  301


 Score = 68.6 bits (166),  Expect = 4e-10, Method: Compositional matrix adjust.
 Identities = 14/212 (6%), Positives = 27/212 (12%), Gaps = 41/212 (19%)

Query  134  PCPRLGHSFSLVG-NKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPIT  192
                           K                   +        +L           P  
Sbjct  44   AREDRQKEREQNEKKKLEEATS---------LETAKKQMASSSEQLL----SRYPLHPEH  90

Query  193  YGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSGCRL-GDLWTLDIDTLTWNKPSLSGVA  251
                    E               K+     +   +  G    L      + K       
Sbjct  91   TYAPNDRLERAIKGA-----LDTFKMGGVLVLHAPQHSGKSAALREVVRGYQKTKRFNAI  145

Query  252  PLPRSLHSAT-TIGNKMYVFGGWVPLVMD---DVKVATHEKEWKCTNTLACLNLDTMAWE  307
             L +   +    I  +           +             E                  
Sbjct  146  LLRKLDITKEDGITGEFTTALQDAC-GVPHDSKATWLPDFFE---LPP----------NN  191

Query  308  TILMDTLEDNIPRARAGHCAVAINTRLYIWSG  339
                      + +        A  T L  +  
Sbjct  192  E---PKNIIILDQFDDDLPYTASATELRGFVT  220


 Score = 55.8 bits (133),  Expect = 3e-06, Method: Compositional matrix adjust.
 Identities = 5/122 (4%), Positives = 19/122 (15%), Gaps = 8/122 (6%)

Query  26   SGPVPRPRHGHRAVAIKELIVVFGGGNEGIV-DELHVYNTATNQWFIPAVRGDIPPGCAA  84
               + +        A    +  F             +     +             G   
Sbjct  196  IIILDQFDDDLPYTASATELRGFVTSLATTAQANKLIVLIGVSDDAYARTMLACNGGTKI  255

Query  85   YGFVCDGTRLLVFGGMVEYGKY---SNDLYELQASRWEWKRLKAKTPKNGPPPCPRLGHS  141
                 +      F  + E        +   ++       ++ +             +   
Sbjct  256  RSIWRNIKDGKNFKWLEEELVKVLDKHYQEDILFHELVDEKARC----KEGNGTSEVSQG  311

Query  142  FS  143
              
Sbjct  312  KI  313


>lcl|m.146441 g.146441  ORF g.146441 m.146441 type:complete len:950 (-) comp16240_c0_seq2:591-3440(-)
Length=950

 Score = 96.3 bits (238),  Expect = 2e-18, Method: Compositional matrix adjust.
 Identities = 7/258 (2%), Positives = 27/258 (10%), Gaps = 39/258 (15%)

Query  29   VPRPRHGHRAVAIKE---LIVVFGGG-NEGIVDELHVYNTATNQWFIPAVRGDIPPGCAA  84
                         +                I   +       +      +  D+      
Sbjct  321  GRDHTAKF-KDTDRLYHLKRETDLTALRNEIEKRMVASVRTGDVVSATTLALDLRGPGLQ  379

Query  85   YGFVCDGTRLLVFGGMVEY-----GKYSNDLYELQASRWEWKRLKAKTPKNGPPPCPRLG  139
               + D   ++                +     +                          
Sbjct  380  PMVLVDLPGIIQHHTKGMPGTTKGAILAMCETHINNPNSII----LCVQDASRDAEASSV  435

Query  140  HSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVLPPP  199
                   +                  + +      +                        
Sbjct  436  ADLVRQADPEGDRTVF---------VLTKVDLAEKLKISSAKL-KSVLQGD---RFNMHA  482

Query  200  RESHTAVVYTEKDNKKSKLVIYGGMSG-CRLGDLWTLDIDTLTW----NKPSLSGVAPLP  254
            R     V  T        +               +  +    +     +  S        
Sbjct  483  RAYFAVVTGTAN--PDDSIEHIRNAERKYFQDSGFFKEGVFSSRRAGTDNLSRLV-----  535

Query  255  RSLHSATTIGNKMYVFGG  272
              +               
Sbjct  536  SDIFWERVRETVALEAAE  553


 Score = 61.6 bits (148),  Expect = 6e-08, Method: Compositional matrix adjust.
 Identities = 13/215 (6%), Positives = 24/215 (11%), Gaps = 36/215 (16%)

Query  134  PCPRLGHSFSLVGNKCYLFGGLANDSE--DPKNNIPRYLNDLYILELRPGSGVVAWDIPI  191
                       V        G  +  E        PR                    I +
Sbjct  267  RSFAAQERLPRVVVVGDQSAGKTSVLEVLVRARIFPRGA---------GEMMTR-APIQV  316

Query  192  TYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSGCRLG-DLWTLDIDTLTWNKPSLSGV  250
            T                        K           +   +          +  +L+  
Sbjct  317  TLSEGRDHTAKFKDTDRLYH----LKRETDLTALRNEIEKRMVASVRTGDVVSATTLALD  372

Query  251  APLPRSLHSATTIGNKMYVFGGWVPL---VMDDVKVATHEKEWKCTNTLACLNLDTMAWE  307
               P            +                                  +N       
Sbjct  373  LRGPGLQPMVLVDLPGIIQHHTKGMPGTTKGAI-----------LAMCETHINNPNSIIL  421

Query  308  T-ILMDTLEDNIPRARAGHCAVAINTRLYIWSGRD  341
                          A +    V         +   
Sbjct  422  CVQDASRD----AEASSVADLVRQADPEGDRTVFV  452


 Score = 56.6 bits (135),  Expect = 2e-06, Method: Compositional matrix adjust.
 Identities = 5/158 (3%), Positives = 19/158 (12%), Gaps = 31/158 (19%)

Query  134  PCPRLGHSFSLVGNK--CYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPI  191
                     +                               +                  
Sbjct  322  RDHTAKFKDTDRLYHLKRETDLTALR---------NEIEKRMVASVR----TGDVVSATT  368

Query  192  TYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGM--------SGCRLGDLWTLDIDTLTWN  243
                L  P      +V          ++ +           +         ++       
Sbjct  369  LALDLRGPGLQPMVLVD------LPGIIQHHTKGMPGTTKGAILA-MCETHINNPNSIIL  421

Query  244  KPSLSGVAPLPRSLHSATTIGNKMYVFGGW-VPLVMDD  280
                +       S+       +       + +  V   
Sbjct  422  CVQDASRDAEASSVADLVRQADPEGDRTVFVLTKVDLA  459


>lcl|m.163194 g.163194  ORF g.163194 m.163194 type:internal len:520 (-) comp21027_c2_seq2:2-1561(-)
Length=520

 Score = 93.6 bits (231),  Expect = 1e-17, Method: Compositional matrix adjust.
 Identities = 72/263 (27%), Positives = 112/263 (42%), Gaps = 38/263 (14%)

Query  23   VGWS-----GPVPRPRHGHRAVAIKELIVVFGGGN-EGIVDELHVYNTATNQWFIPAVRG  76
            V WS     G VP  R GH    +    V+FGGG+ +   ++++ ++ AT+ W     RG
Sbjct  178  VHWSEVACTGEVPSAREGHSFNFVNGSFVLFGGGSAQQEFNDIYTFDPATSSWKKATGRG  237

Query  77   DIPPGCAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGPPPCP  136
              P     +      T L V GG  + G+   D ++L     +W  L          P  
Sbjct  238  TAPAARLNHAAAVIDTALFVHGGFQD-GQAVADFFKLDTRTMQWTNL-----PTAHGPSA  291

Query  137  RLGHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAWDIPITYGVL  196
            R  H+ +  G K Y+FGG A D++          NDL + +   G     W      G  
Sbjct  292  RCNHTCTAFGQKLYVFGGRAGDNQ--------LFNDLTVFDTVKGE----WSAVNAKGTP  339

Query  197  PPPRESHTAVVYTEKDNKKSKLVIYGG-------MSGCRLGDLWTLDIDTLTWNKPSLSG  249
            P  R+ H+AV   +       +V++GG        +     D+   D +T TW K +++ 
Sbjct  340  PDARDFHSAVAVGK------TMVVFGGSQEIASSNTSIIYNDVHAFDTETQTWRKLAVAD  393

Query  250  VAPLPRSLHSATTIGNKMYVFGG  272
              P+ R  H  T +G+KM+VFGG
Sbjct  394  SPPV-RWAHGCTVVGSKMFVFGG  415


 Score = 73.6 bits (179),  Expect = 2e-11, Method: Compositional matrix adjust.
 Identities = 58/275 (21%), Positives = 109/275 (39%), Gaps = 41/275 (14%)

Query  68   QWFIPAVRGDIPPGCAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKT  127
             W   A  G++P     + F       ++FGG     ++ ND+Y    +   WK+   + 
Sbjct  179  HWSEVACTGEVPSAREGHSFNFVNGSFVLFGGGSAQQEF-NDIYTFDPATSSWKKATGR-  236

Query  128  PKNGPPPCPRLGHSFSLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRPGSGVVAW  187
               G  P  RL H+ +++    ++ GG  +          + + D + L+ R     + W
Sbjct  237  ---GTAPAARLNHAAAVIDTALFVHGGFQDG---------QAVADFFKLDTR----TMQW  280

Query  188  -DIPITYGVLPPPRESHTAVVYTEKDNKKSKLVIYGGMSGCR--LGDLWTLDIDTLTWNK  244
             ++P  +G  P  R +HT   + +K      L ++GG +G      DL   D     W+ 
Sbjct  281  TNLPTAHG--PSARCNHTCTAFGQK------LYVFGGRAGDNQLFNDLTVFDTVKGEWSA  332

Query  245  PSLSGVAPLPRSLHSATTIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDTM  304
             +  G  P  R  HSA  +G  M VFGG   +   +  +          N +   + +T 
Sbjct  333  VNAKGTPPDARDFHSAVAVGKTMVVFGGSQEIASSNTSII--------YNDVHAFDTETQ  384

Query  305  AWETILMDTLEDNIPRARAGHCAVAINTRLYIWSG  339
             W  + +     + P  R  H    + ++++++ G
Sbjct  385  TWRKLAV----ADSPPVRWAHGCTVVGSKMFVFGG  415


 Score = 70.1 bits (170),  Expect = 2e-10, Method: Compositional matrix adjust.
 Identities = 61/208 (29%), Positives = 92/208 (44%), Gaps = 35/208 (16%)

Query  146  GNKCYLFGGLANDSE-DPKNNIPRYLNDLYILE-LR-PGSGVVAWDIPITYGVLPPPRES  202
            GN+ ++FGG A ++E DP +       D++ L  LR P S  V W      G +P  RE 
Sbjct  143  GNQAFVFGGTATNAEGDPYSR-----QDMFQLSGLRTPTS--VHWSEVACTGEVPSAREG  195

Query  203  HTAVVYTEKDNKKSKLVIYGGMSGCR-LGDLWTLDIDTLTWNKPSLSGVAPLPRSLHSAT  261
            H+       +      V++GG S  +   D++T D  T +W K +  G AP  R  H+A 
Sbjct  196  HSF------NFVNGSFVLFGGGSAQQEFNDIYTFDPATSSWKKATGRGTAPAARLNHAAA  249

Query  262  TIGNKMYVFGGWVPLVMDDVKVATHEKEWKCTNTLACLNLDTMAWETILMDTLEDNIPRA  321
             I   ++V GG+     D   VA   K          L+  TM W  +       + P A
Sbjct  250  VIDTALFVHGGF----QDGQAVADFFK----------LDTRTMQWTNLPT----AHGPSA  291

Query  322  RAGHCAVAINTRLYIWSGRDGYRKAWNN  349
            R  H   A   +LY++ GR G  + +N+
Sbjct  292  RCNHTCTAFGQKLYVFGGRAGDNQLFND  319


 Score = 64.7 bits (156),  Expect = 7e-09, Method: Compositional matrix adjust.
 Identities = 43/158 (27%), Positives = 70/158 (44%), Gaps = 20/158 (12%)

Query  30   PRPRHGHRAVAIKELIVVFGG--GNEGIVDELHVYNTATNQWFIPAVRGDIPPGCAAYGF  87
            P  R  H   A  + + VFGG  G+  + ++L V++T   +W     +G  P     +  
Sbjct  289  PSARCNHTCTAFGQKLYVFGGRAGDNQLFNDLTVFDTVKGEWSAVNAKGTPPDARDFHSA  348

Query  88   VCDGTRLLVFGGMVEYGKYS-----NDLYELQASRWEWKRLKAKTPKNGPPPCPRLGHSF  142
            V  G  ++VFGG  E    +     ND++        W++L      + PP   R  H  
Sbjct  349  VAVGKTMVVFGGSQEIASSNTSIIYNDVHAFDTETQTWRKLAV---ADSPP--VRWAHGC  403

Query  143  SLVGNKCYLFGGLANDSEDPKNNIPRYLNDLYILELRP  180
            ++VG+K ++FGG  +D  D        L DL++L   P
Sbjct  404  TVVGSKMFVFGGTGSDGRD--------LADLHVLTFEP  433


>lcl|m.10928 g.10928  ORF g.10928 m.10928 type:5prime_partial len:73 (-) comp7185_c0_seq1:357-575(-)
Length=73

 Score = 93.6 bits (231),  Expect = 1e-17, Method: Composition-based stats.
 Identities = 0/282 (0%), Positives = 1/282 (0%), Gaps = 33/282 (11%)

Query  16   QPRWKRVVGWSGPVPRPRHGHRAVAIKELIVVFGGGNE---GIVDELHVYNTATNQWFIP  72
                +     
Sbjct  65   VHVPQET-A�  123

Query  73   AVRGDIPPGCAAYGFVCDGTRLLVFGGMVEYGKYSNDLYELQASRWEWKRLKAKTPKNGP  132```